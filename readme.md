## Raspberry Pi 4 based Self-Balancing Robot

# main_pid.py
Main program for the self-balancing robot. This uses PID controller to balance and to maintain position. 
Necessary files:
* kalman.py
* moving_average_filter.py
* stepper.py
* mpu6050.py

Ongoing project. The parameters require tuning. 

# main.py
Main program with the Q-learning implementation for self-balancing. 
Necessary files: 
* kalman.py
* moving_average_filter.py
* stepper.py
* mpu6050.py
* qlearning2d.py
* reward_function.py