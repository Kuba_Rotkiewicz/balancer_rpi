"""
This program handles the communication over I2C
between a Raspberry Pi and a MPU-6050 Gyroscope / Accelerometer combo.
Made by: 
Released under the MIT License
Copyright (c) 2015, 2016, 2017 MrTijn/Tijndagamer
"""

import smbus
from time import sleep
import numpy as np
#from math import atan, pi

#import threading

#import matplotlib.pyplot as plt
#import numpy as np
#import sys
#from moving_average_filter import MovingAverageFilter

class MPU6050:
    # Global Variables
    GRAVITIY_MS2 = 9.80665
    #address = None
    #bus = None

    # Scale Modifiers
    ACCEL_SCALE_MODIFIER_2G = 16384.0
    ACCEL_SCALE_MODIFIER_4G = 8192.0
    ACCEL_SCALE_MODIFIER_8G = 4096.0
    ACCEL_SCALE_MODIFIER_16G = 2048.0

    GYRO_SCALE_MODIFIER_250DEG = 131.0
    GYRO_SCALE_MODIFIER_500DEG = 65.5
    GYRO_SCALE_MODIFIER_1000DEG = 32.8
    GYRO_SCALE_MODIFIER_2000DEG = 16.4

    AccScale = ACCEL_SCALE_MODIFIER_2G
    GyrScale = GYRO_SCALE_MODIFIER_250DEG

    # Pre-defined ranges
    ACCEL_RANGE_2G = 0x00
    ACCEL_RANGE_4G = 0x08
    ACCEL_RANGE_8G = 0x10
    ACCEL_RANGE_16G = 0x18

    GYRO_RANGE_250DEG = 0x00
    GYRO_RANGE_500DEG = 0x08
    GYRO_RANGE_1000DEG = 0x10
    GYRO_RANGE_2000DEG = 0x18

    # MPU-6050 Registers

    PWR_MGMT_1 = 0x6B
    """ Possible clocks """
    CLOCK_INTERNAL = 0x00
    CLOCK_PLL_XGYRO = 0x01
    CLOCK_PLL_YGYRO = 0x02
    CLOCK_PLL_ZGYRO = 0x03
    CLOCK_PLL_EXT32K = 0x04
    CLOCK_PLL_EXT19M = 0x05
    CLOCK_KEEP_RESET = 0x07

    PWR_MGMT_2 = 0x6C

    SIGNAL_PATH_RESET = 0x68

    ACCEL_XOUT0 = 0x3B
    ACCEL_YOUT0 = 0x3D
    ACCEL_ZOUT0 = 0x3F

    TEMP_OUT0 = 0x41

    GYRO_XOUT0 = 0x43
    GYRO_YOUT0 = 0x45
    GYRO_ZOUT0 = 0x47

    ACCEL_CONFIG = 0x1C
    GYRO_CONFIG = 0x1B
    CONFIG = 0x1A
    """ Possible cut-off frequencies """
    DLPF_BW_256 = 0x00
    DLPF_BW_188 = 0x01
    DLPF_BW_98 = 0x02
    DLPF_BW_42 = 0x03
    DLPF_BW_20 = 0x04
    DLPF_BW_10 = 0x05
    DLPF_BW_5 = 0x06

    XA_OFFS_H = 0x06        #[15:0] XA_OFFS
    XA_OFFS_L = 0x07
    YA_OFFS_H = 0x08        #[15:0] YA_OFFS
    YA_OFFS_L = 0x09
    ZA_OFFS_H = 0x0A        #[15:0] ZA_OFFS
    ZA_OFFS_L = 0x0B

    XG_OFFS_USRH = 0x13     #[15:0] XG_OFFS_USR
    XG_OFFS_USRL = 0x14
    YG_OFFS_USRH = 0x15     #[15:0] YG_OFFS_USR
    YG_OFFS_USRL = 0x16
    ZG_OFFS_USRH = 0x17     #[15:0] ZG_OFFS_USR
    ZG_OFFS_USRL = 0x18


    def __init__(self, address=0x68, bus=1):
        self.address = address
        self.bus = smbus.SMBus(bus)
        # Wake up the MPU-6050 since it starts in sleep mode
        self.bus.write_byte_data(self.address, self.PWR_MGMT_1, 0x00)
        sleep(0.1)
        self.signal_paths_reset()
        sleep(0.1)
        self.set_clock_source(self.CLOCK_PLL_XGYRO)
        self.set_dlfp(self.DLPF_BW_5)
        self.set_accel_range(self.ACCEL_RANGE_2G)
        self.set_gyro_range(self.GYRO_RANGE_250DEG)
        self.set_x_acc_offset(-2587)
        self.set_y_acc_offset(1015)
        self.set_z_acc_offset(705)
        self.set_x_gyro_offset(34)
        self.set_y_gyro_offset(16)
        self.set_z_gyro_offset(-7)

    # I2C communication methods

    def signal_paths_reset(self):
        tmp = self.bus.read_byte_data(self.address, self.SIGNAL_PATH_RESET)
        tmp = tmp | 0x01
        self.bus.write_byte_data(self.address, self.SIGNAL_PATH_RESET, tmp)

    def set_clock_source(self, source):
        tmp = self.bus.read_byte_data(self.address, self.PWR_MGMT_1)
        tmp &= 0xF8
        tmp |= (source & 0x7)
        self.bus.write_byte_data(self.address, self.PWR_MGMT_1, tmp)

    def set_dlfp(self, value):
        """ Digital Low Pass Filter """
        tmp = self.bus.read_byte_data(self.address, self.CONFIG)
        tmp &= 0xF8
        tmp |= (value & 0x7)
        self.bus.write_byte_data(self.address, self.CONFIG, tmp)

    def read_i2c_word(self, register):
        """Read two i2c registers and combine them.
        register -- the first register to read from.
        Returns the combined read results.
        """
        # Read the data from the registers
        try:
            high = self.bus.read_byte_data(self.address, register)
            low = self.bus.read_byte_data(self.address, register + 1)
        except:
            high = 0
            low = 1
            print("Can not read i2c bus")

        value = (high << 8) + low

        if (value >= 0x8000):
            return -((65535 - value) + 1)
        else:
            return value

    # MPU-6050 Methods

    def get_temp(self):
        """Reads the temperature from the onboard temperature sensor of the MPU-6050.
        Returns the temperature in degrees Celcius.
        """
        raw_temp = self.read_i2c_word(self.TEMP_OUT0)

        # Get the actual temperature using the formule given in the
        # MPU-6050 Register Map and Descriptions revision 4.2, page 30
        actual_temp = (raw_temp / 340.0) + 36.53

        return actual_temp

    def set_accel_range(self, accel_range):
        """Sets the range of the accelerometer to range.
        accel_range -- the range to set the accelerometer to. Using a
        pre-defined range is advised.
        """
        # First change it to 0x00 to make sure we write the correct value later
        self.bus.write_byte_data(self.address, self.ACCEL_CONFIG, 0x00)

        # Write the new range to the ACCEL_CONFIG register
        self.bus.write_byte_data(self.address, self.ACCEL_CONFIG, accel_range)

        if accel_range == self.ACCEL_RANGE_2G:
            self.AccScale = self.ACCEL_SCALE_MODIFIER_2G
        elif accel_range == self.ACCEL_RANGE_4G:
            self.AccScale = self.ACCEL_SCALE_MODIFIER_4G
        elif accel_range == self.ACCEL_RANGE_8G:
            self.AccScale = self.ACCEL_SCALE_MODIFIER_8G
        elif accel_range == self.ACCEL_RANGE_16G:
            self.AccScale = self.ACCEL_SCALE_MODIFIER_16G

    def get_accel_data(self):   #, g = False
        """Gets and returns the X, Y and Z values from the accelerometer.
        If g is True, it will return the data in g
        If g is False, it will return the data in m/s^2
        Returns a dictionary with the measurement results.
        """
        x = self.read_i2c_word(self.ACCEL_XOUT0)
        y = self.read_i2c_word(self.ACCEL_YOUT0)
        z = self.read_i2c_word(self.ACCEL_ZOUT0)
        
        x = x / self.AccScale
        y = y / self.AccScale
        z = z / self.AccScale

        return {'x': x, 'y': y, 'z': z}
        """
        if g is True:
            return {'x': x, 'y': y, 'z': z}
        elif g is False:
            x = x * self.GRAVITIY_MS2
            y = y * self.GRAVITIY_MS2
            z = z * self.GRAVITIY_MS2
            return {'x': x, 'y': y, 'z': z}
        """

    def get_accel_x_data(self):
        x = self.read_i2c_word(self.ACCEL_XOUT0)
        x = x / self.AccScale
        return x

    def get_accel_y_data(self):
        y = self.read_i2c_word(self.ACCEL_YOUT0)
        y = y / self.AccScale
        return y

    def get_accel_z_data(self):
        z = self.read_i2c_word(self.ACCEL_ZOUT0)
        z = z / self.AccScale
        return z

    def set_gyro_range(self, gyro_range):
        """Sets the range of the gyroscope to range.
        gyro_range -- the range to set the gyroscope to. Using a pre-defined
        range is advised.
        """
        # First change it to 0x00 to make sure we write the correct value later
        self.bus.write_byte_data(self.address, self.GYRO_CONFIG, 0x00)

        # Write the new range to the ACCEL_CONFIG register
        self.bus.write_byte_data(self.address, self.GYRO_CONFIG, gyro_range)

        if gyro_range == self.GYRO_RANGE_250DEG:
            self.GyrScale = self.GYRO_SCALE_MODIFIER_250DEG
        elif gyro_range == self.GYRO_RANGE_500DEG:
            self.GyrScale = self.GYRO_SCALE_MODIFIER_500DEG
        elif gyro_range == self.GYRO_RANGE_1000DEG:
            self.GyrScale = self.GYRO_SCALE_MODIFIER_1000DEG
        elif gyro_range == self.GYRO_RANGE_2000DEG:
            self.GyrScale = self.GYRO_SCALE_MODIFIER_2000DEG

    def get_gyro_data(self):
        """Gets and returns the X, Y and Z values from the gyroscope.
        Returns the read values in a dictionary.
        """
        x = self.read_i2c_word(self.GYRO_XOUT0)
        y = self.read_i2c_word(self.GYRO_YOUT0)
        z = self.read_i2c_word(self.GYRO_ZOUT0)
        
        x = x / self.GyrScale
        y = y / self.GyrScale
        z = z / self.GyrScale

        return {'x': x, 'y': y, 'z': z}

    def get_gyro_x_data(self):
        x = self.read_i2c_word(self.GYRO_XOUT0)
        x = x / self.GyrScale
        return x

    def get_gyro_y_data(self):
        y = self.read_i2c_word(self.GYRO_YOUT0)
        y = y / self.GyrScale
        return y

    def get_gyro_z_data(self):
        z = self.read_i2c_word(self.GYRO_ZOUT0)
        z = z / self.GyrScale
        return z

    def get_all_data(self):
        """Reads and returns all the available data."""
        temp = self.get_temp()
        accel = self.get_accel_data()
        gyro = self.get_gyro_data()

        return [accel, gyro, temp]

    def set_x_acc_offset(self, offset):
        #print("Offsets:")
        #print("Ax:",self.read_i2c_word(self.XA_OFFS_H))
        self.bus.write_byte_data(self.address, self.XA_OFFS_H, offset>>8)
        self.bus.write_byte_data(self.address, self.XA_OFFS_L, offset)
        #print(self.read_i2c_word(self.XA_OFFS_H))

    def set_y_acc_offset(self, offset):
        #print("Ay:",self.read_i2c_word(self.YA_OFFS_H))
        self.bus.write_byte_data(self.address, self.YA_OFFS_H, offset>>8)
        self.bus.write_byte_data(self.address, self.YA_OFFS_L, offset)
        #print(self.read_i2c_word(self.YA_OFFS_H))

    def set_z_acc_offset(self, offset):
        #print("Az:",self.read_i2c_word(self.ZA_OFFS_H))
        self.bus.write_byte_data(self.address, self.ZA_OFFS_H, offset>>8)
        self.bus.write_byte_data(self.address, self.ZA_OFFS_L, offset)
        #print(self.read_i2c_word(self.ZA_OFFS_H))

    def set_x_gyro_offset(self, offset):
        #print("Gx:",self.read_i2c_word(self.XG_OFFS_USRH))
        self.bus.write_byte_data(self.address, self.XG_OFFS_USRH, offset>>8)
        self.bus.write_byte_data(self.address, self.XG_OFFS_USRL, offset)
        #print(self.read_i2c_word(self.XG_OFFS_USRH))

    def set_y_gyro_offset(self, offset):
        #print("Gy:",self.read_i2c_word(self.YG_OFFS_USRH))
        self.bus.write_byte_data(self.address, self.YG_OFFS_USRH, offset>>8)
        self.bus.write_byte_data(self.address, self.YG_OFFS_USRL, offset)
        #print(self.read_i2c_word(self.YG_OFFS_USRH))

    def set_z_gyro_offset(self, offset):
        #print("Gz:",self.read_i2c_word(self.ZG_OFFS_USRH))
        self.bus.write_byte_data(self.address, self.ZG_OFFS_USRH, offset>>8)
        self.bus.write_byte_data(self.address, self.ZG_OFFS_USRL, offset)
        #print(self.read_i2c_word(self.ZG_OFFS_USRH))

    def get_pitch_angle(self):
        acc = self.get_accel_data()
        pitch = np.arctan(acc['x']/(acc['y'] * acc['y'] + acc['z'] * acc['z'])) * 180.0 / np.pi
        return pitch

def handler(mpu, my_f):
    global pitch
    global i
    #global pitch_array
    while True:
        pitch = mpu.get_pitch_angle()
        #filtered = np.convolve(pitch, np.ones(20)/20, mode='valid')
        #filtered, pitch_array = moving_average_filter(pitch_array, pitch, 20)
        filtered = my_f.moving_average_filter(pitch)
        #print(f"Angle: {(pitch):9.4f}, Filtered: {(filtered):9.4f}")
        print(f"Angle: {(pitch):9.4f}")
        #i+=1
        sleep(0.005)

if __name__ == "__main__":
    try:
        ONLINE_PLOT = 0
        #global mpu
        mpu = MPU6050()
        global my_f
        my_f = MovingAverageFilter(20)

        t = threading.Thread(target=handler, args=(mpu, my_f))
        t.start()

        if ONLINE_PLOT == 1:
            global x
            global y
            i=0
            x=[]
            y=[]
            i_old = 0
            plt.ion()
            fig=plt.figure()
            plt.axis([0,1000,-40, 40])

            while True: 
                if (i_old != i):
                    x.append(i)
                    y.append(pitch)
                    plt.scatter(i,pitch)
                    i_old = i
                    plt.draw()
                    plt.pause(0.0001)

    except KeyboardInterrupt():
        print("Interrupt!")
    finally:
        sys.exit()

"""    
    while True:
        
        print("Temperature: ", mpu.get_temp())
        accel_data = mpu.get_accel_data()
        print("Acc X: ", accel_data['x'])
        print("Acc Y: ", accel_data['y'])
        print("Acc Z: ", accel_data['z'])
        gyro_data = mpu.get_gyro_data()
        print("Gyr X: ", gyro_data['x'])
        print("Gyr Y: ", gyro_data['y'])
        print("Gyr Z: ", gyro_data['z'])
      
        pitch = mpu.get_pitch_angle()
        print("Pitch: ", pitch)

        sleep(0.01)  
"""