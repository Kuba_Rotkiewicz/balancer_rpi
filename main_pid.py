import numpy as np
import sys
from moving_average_filter import MovingAverageFilter
from mpu6050 import MPU6050
from kalman import Kalman
import csv
import time
import traceback
from simple_pid import PID
from stepper import Stepper

def calibrate_setpoint_angle(read_angle_period):
    """
    Function to calibrate initial setpoint angle. 
    The equilibrium point, so the balance state, is the angle = 0, 
    but physically it is impossible to mount IMU with exactly angle value = 0.
    That's why the initial angle is measured, called offset_angle, 
    and this particular angle is later treated as equilibrium point.
    After turning on the user should hold the robot vertically, within equilibrium point,
    for the time of calibration.
    Here the offset angle is not measured every time, it was measured once and it is hard coded below. 
    """
    offset_angle_array = []
    print("Calibration...")
    while len(offset_angle_array) < 100:
        pitch = mpu.get_pitch_angle()
        offset_angle_array = np.append(offset_angle_array, pitch)
        time.sleep(read_angle_period)

    offset_angle = sum(offset_angle_array[-50:])/50
    offset_angle_array = np.append(offset_angle_array, offset_angle)
    print('Offset Angle: ',offset_angle)
    return offset_angle

def get_angle(angle_dt):
    """
    Function performs actions to get final angle:
    - read the accelerometer and calculate the pitch angle
    - read the gyroscope and calculate the angular velocity
    - filter the pitch with moving average filter
    - run kalman filter to merge pitch and angular velocity 
      into stable and precise angle value in degrees
    """
    #angle_timer = time.time()
    pitch = mpu.get_pitch_angle()
    #pitch_dt = time.time() - angle_timer
    angular_velocity = -mpu.get_gyro_y_data()
    #ang_vel_dt = time.time() - pitch_dt - angle_timer
    pitch_filtered = f.moving_average_filter_conv(pitch)
    #filt_dt = time.time() - ang_vel_dt - pitch_dt - angle_timer
    #angle = (1 - alpha) * (angle + angular_velocity * angle_dt)+ (alpha) * (pitch_filtered);       
    angle = k.getAngle(pitch, angular_velocity, angle_dt)
    #angle_dt = time.time() - filt_dt- ang_vel_dt - pitch_dt - angle_timer
    #print(f'Angle: {(pitch):9.4f}, pitch_filtered: {(pitch_filtered):9.4f}, Time: {(angle_dt):9.4f}, Kalman: {(angle):9.4f}')
    #print(f'p_dt:{pitch_dt:5.4f},a_v_dt:{ang_vel_dt:5.4f},f_dt:{filt_dt:5.4f},a_dt:{angle_dt:5.4f}')
    return angle

READ_ANGLE_FREQUENCY = 50
READ_ANGLE_PERIOD = round(1/READ_ANGLE_FREQUENCY,3)

CALC_PID_FREQUENCY = 200
CALC_PID_PERIOD = round(1/CALC_PID_FREQUENCY,3)
CONTROL_FREQUENCY_RESOLUTION = 50 # Control signal discretized to (0,50,100,...)

CALC_PID_POS_FREQUENCY = 200
CALC_PID_POS_PERIOD = round(1/CALC_PID_POS_FREQUENCY,3)

POSITION_HOLD = 1

""" Initialize parameters """
angle = 0
pitch = 0
angular_velocity = 0
pitch_filtered = 0
offset_angle = 0
fall_trigger = 1
calibration_time_counter = 0
left_freq_old = 0
right_freq_old = 0
left_wheel_position = 0
right_wheel_position = 0
left_wheel_position_pid = 0
right_wheel_position_pid = 0

""" Initialize MPU6050 """ 
mpu = MPU6050()

""" Initial setpoint angle calibration """
offset_angle = calibrate_setpoint_angle(READ_ANGLE_PERIOD)
#offset_angle = 2.3845883137902093

""" Initialize pitch angle moving average filter """
f = MovingAverageFilter(22) 

""" Initialize Kalman filter - get initial angle from measuring pitch """
angle_init = mpu.get_pitch_angle()
k = Kalman(0.001, 0.003, 0.2, angle_init)

""" Initialize stepper motors """
sm = Stepper()
right_wheel_status = 1
left_wheel_status = 1

# Initialize PID - angle and position
P = 1000.0
I = 0.0
D = 2.0
pid_left_wheel_velocity = PID(P, I, D, offset_angle, CALC_PID_PERIOD, proportional_on_measurement=False)
pid_right_wheel_velocity = PID(P, I, D, offset_angle, CALC_PID_PERIOD, proportional_on_measurement=False)
pid_left_wheel_velocity.output_limits = (-28000, 28000)
pid_right_wheel_velocity.output_limits = (-28000, 28000)

if POSITION_HOLD:
    P_pos = 0.35
    I_pos = 0.000
    D_pos = 0.000
    pid_left_wheel_setpoint_angle = PID(P_pos, I_pos, D_pos, 0, CALC_PID_POS_PERIOD, proportional_on_measurement=False)
    pid_right_wheel_setpoint_angle = PID(P_pos, I_pos, D_pos, 0, CALC_PID_POS_PERIOD, proportional_on_measurement=False)
    pid_left_wheel_setpoint_angle.output_limits = (-20.0, 20.0)
    pid_right_wheel_setpoint_angle.output_limits = (-20.0, 20.0)


""" Attempt to self stand up routine 
PID_ENABLE = 0
WHEELS_ENABLE = 0
get_up_freq = 200
"""

""" Create files to save data """
with open("Data/angle_pid.csv", 'w') as file:
    file.write(f"Angle,dt,{offset_angle}\n")

with open("Data/pid.csv", 'w') as file:
    file.write("freq_left,freq_right,dt\n")

""" Start timers """
angle_timer = time.time()
pid_timer = time.time()
pid_pos_timer = time.time()

try:
    while True:
        """ Angle derivation - loop every READ_ANGLE_PERIOD """
        angle_dt = time.time()-angle_timer
        if(angle_dt > READ_ANGLE_PERIOD):
            angle_timer = time.time()

            angle = get_angle(angle_dt)
            with open("Data/angle_pid.csv", 'a') as file:
                file.write(f"{angle},{angle_dt}\n")

            """ Self stand-up routine 
            if(angle < offset_angle and PID_ENABLE == 0): #0 < angle - offset_angle < 2.8
                PID_ENABLE = 1
                WHEELS_ENABLE = 0

            if WHEELS_ENABLE == 1:
                get_up_freq = get_up_freq + 40
                sm.right_wheel_freq(get_up_freq)
                sm.left_wheel_freq(get_up_freq)
                if(get_up_freq >= 3700):
                    sm.right_wheel_freq(100)
                    sm.left_wheel_freq(100)
            """
            if calibration_time_counter < 20:
                calibration_time_counter += 1
                if calibration_time_counter == 20:
                    sm.right_wheel_enable()
                    sm.left_wheel_enable()
                    left_wheel_position_pid = 0
                    right_wheel_position_pid = 0

                    """ Self stand-up routine 
                    if angle > offset_angle:
                        sm.left_wheel_backward()
                        sm.right_wheel_backward()
                    else :
                        sm.left_wheel_forward()
                        sm.right_wheel_forward()
                    WHEELS_ENABLE = 1
                    """

        """ Self stand-up routine
        if(0 < mpu.get_pitch_angle() - offset_angle < 1 and PID_ENABLE == 0):
            PID_ENABLE = 1
            WHEELS_ENABLE = 0
        """

        """ PID for angle control - loop every CALC_PID_PERIOD """
        pid_dt = time.time() - pid_timer
        if(pid_dt > CALC_PID_PERIOD): 
            pid_timer = time.time()
            #start = time.time()
            
            left_wheel_control_frequency = pid_left_wheel_velocity(angle)
            right_wheel_control_frequency = pid_right_wheel_velocity(angle)

            with open("Data/pid.csv", 'a') as file:
                file.write(f"{left_wheel_control_frequency},{right_wheel_control_frequency},{pid_dt}\n")
            
            #pid_calc_dt = time.time() - pid_timer
            left_freq = round(abs(left_wheel_control_frequency)/CONTROL_FREQUENCY_RESOLUTION)*CONTROL_FREQUENCY_RESOLUTION
            right_freq = round(abs(right_wheel_control_frequency)/CONTROL_FREQUENCY_RESOLUTION)*CONTROL_FREQUENCY_RESOLUTION

            # LEFT WHEEL PID
            if (angle > pid_left_wheel_velocity.setpoint and angle < pid_left_wheel_velocity.setpoint + 70):
                if fall_trigger == 0:
                    sm.left_wheel_enable()
                    fall_trigger = 1
                sm.left_wheel_forward()
                left_wheel_position_pid = left_wheel_position_pid + left_freq * pid_dt * 0.007952
            elif (angle < pid_left_wheel_velocity.setpoint and angle > pid_left_wheel_velocity.setpoint - 70):
                if fall_trigger == 0:
                    sm.left_wheel_enable()
                    fall_trigger = 1
                sm.left_wheel_backward()
                left_wheel_position_pid = left_wheel_position_pid - left_freq * pid_dt * 0.007952
            else:
                sm.left_wheel_disable()
                sm.right_wheel_disable()
                fall_trigger = 0

            if (left_freq != left_freq_old and fall_trigger != 0):
                while(left_wheel_status != 0):
                    left_wheel_status = sm.left_wheel_freq(left_freq)
                    #print(left_wheel_status)
                left_wheel_status = 1
                left_freq_old = left_freq

            # RIGHT WHEEL PID
            if (angle > pid_right_wheel_velocity.setpoint and angle < pid_right_wheel_velocity.setpoint + 70):
                if fall_trigger == 0:
                    sm.right_wheel_enable()
                    fall_trigger = 1
                sm.right_wheel_forward()
                right_wheel_position_pid = right_wheel_position_pid + right_freq * pid_dt * 0.007952
            elif (angle < pid_right_wheel_velocity.setpoint and angle > pid_right_wheel_velocity.setpoint - 70):
                if fall_trigger == 0:
                    sm.right_wheel_enable()
                    fall_trigger = 1
                sm.right_wheel_backward()
                right_wheel_position_pid = right_wheel_position_pid - right_freq * pid_dt * 0.007952
            else:
                sm.left_wheel_disable()
                sm.right_wheel_disable()
                fall_trigger = 0

            if (right_freq != right_freq_old and fall_trigger != 0):
                while(right_wheel_status != 0):
                    right_wheel_status = sm.right_wheel_freq(right_freq)
                    #print(right_wheel_status)
                right_wheel_status = 1
                right_freq_old = right_freq

            #print(f'Left position:{left_wheel_position:5.4f}, right position:{right_wheel_position:5.4f}')      
            #print(f'pid_dt:{pid_dt:5.4f},pid_c_dt:{pid_calc_dt:5.4f},f_dt:{forward_dt:5.4f},b_dt:{backward_dt:5.4f}')  
            #print(f'pid_dt:{pid_dt:5.4f},left_freq:{left_freq:5.4f},right_freq:{right_freq:5.4f},f_dt:{forward_dt:5.4f},b_dt:{backward_dt:5.4f}') 
            #end = time.time()
            #dt = end - start

            #print(f'Left: {left_freq}, Right: {right_freq}, dt: {(pid_dt):9.7f}, eq: {left_freq==right_freq}')
            #print(f'Angle dt: {(angle_dt):9.5f}, PID dt: {(pid_dt):9.5f}, PID c_time: {(dt):9.6f}')
            #print(f'Left: {(left_freq):9.4f}, Right: {(right_freq):9.4f}, Equal: {left_freq==right_freq}')
        if POSITION_HOLD:
            """ PID for position control - loop every CALC_PID_POS_PERIOD """
            pid_pos_dt = time.time() - pid_pos_timer
            if(pid_pos_dt > CALC_PID_POS_PERIOD): 
                pid_pos_timer = time.time()
                
                left_wheel_position = sm.get_left_position() 
                right_wheel_position = sm.get_right_position()
                
                left_wheel_control_setpoint_angle = pid_left_wheel_setpoint_angle(-right_wheel_position_pid)
                right_wheel_control_setpoint_angle = pid_right_wheel_setpoint_angle(-left_wheel_position_pid)

                pid_left_wheel_velocity.setpoint = offset_angle - left_wheel_control_setpoint_angle
                pid_right_wheel_velocity.setpoint = offset_angle - right_wheel_control_setpoint_angle
                
                #print(f'stp:{offset_angle:5.4f},ang:{angle:5.4f},L pos:{left_wheel_position:5.4f}, r pos:{right_wheel_position:5.4f}, stptangL :{pid_left_wheel_velocity.setpoint:5.4f}, stptangR:{pid_right_wheel_velocity.setpoint:5.4f}')
                print(f'L pos pid:{left_wheel_position_pid:5.4f},R pos pid:{left_wheel_position_pid:5.4f}, L pos:{left_wheel_position:5.4f}, r pos:{right_wheel_position:5.4f}')


except KeyboardInterrupt():
    print('Interrupt!')
finally:
    sm.right_wheel_freq(0)
    sm.left_wheel_freq(0)
    sm.left_wheel_disable()
    sm.right_wheel_disable()
    sys.exit()