import matplotlib.pyplot as plt
import numpy as np

class RewardFunction():
    def __init__(self, target_range = 5, discrete_states = 21):
        self.discrete_states = discrete_states
        self.target_range = target_range
        self.sigma = np.sqrt(target_range/2)

    def gaussian_function(self, x):
        x_f = x * ((self.target_range*2)/(self.discrete_states-1))
        reward = np.exp(-(x_f**2)/(2*self.sigma**2))
        return reward

    def direction_function(self, state_error, last_state_error, reward):
        """if state_error == last_state_error -> remember and give previous reward """
        if(state_error > last_state_error):
            reward = -1 
        elif(state_error < last_state_error):
            reward = 2
        last_state_error = state_error
        return reward, last_state_error

    def simple_function(self, state_error, max_state_error):
        if(state_error > max_state_error):
            reward = -1
        elif(state_error <= max_state_error):
            reward = 1
        return reward


    def plot_gaussian_function(self):
        #x = np.arange(-self.target_range,self.target_range,(self.target_range*2)/(self.discrete_states-1))
        #x = [-5,-4.5,-4,-3.5,-3,-2.5,-2,-1.5,-1,-0.5,0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5]
        x = [-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10]
        print(x)
        y = np.empty((0))
        for i in x:
            y = np.append(y, self.gaussian_function(i))
        x = [-5,-4.5,-4,-3.5,-3,-2.5,-2,-1.5,-1,-0.5,0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5]
        plt.figure()
        plt.plot(x,y, label="Gaussian function", marker='.', markersize=4, linewidth=1)
        plt.grid()
        plt.title("Reward function")
        plt.xlabel("Angle")
        plt.ylabel("Reward")
        plt.legend()
        plt.show()

if __name__ == "__main__":
    r = RewardFunction()
    r.plot_gaussian_function()