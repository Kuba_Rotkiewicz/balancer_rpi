import numpy as np

class MovingAverageFilter():
    def __init__(self, window_size=10):
        self.window_size = window_size
        self.filter_array = np.zeros(self.window_size)
        self.filter_array_conv = np.zeros(self.window_size)
        #self.filter_array = [0 for i in range(0,self.window_size)]
        #print(self.filter_array)

    def moving_average_filter(self, newValue):
        if(np.isfinite(newValue)):
            array_sum = 0
            for i in range(0,self.window_size-1):
                self.filter_array[self.window_size - 1 - i] = self.filter_array[self.window_size-2-i]
                array_sum = np.sum(self.filter_array)
            self.filter_array[0] = newValue
            newValue = (array_sum + newValue)/self.window_size
            return newValue

    def moving_average_filter_conv(self, newValue):
        if(np.isfinite(newValue)):
            self.filter_array_conv = np.append(self.filter_array_conv, newValue)
            self.filter_array_conv = np.delete(self.filter_array_conv, 0)
            newValue = np.convolve(self.filter_array_conv, np.ones(self.window_size)/self.window_size, mode='valid')
            return newValue[0]