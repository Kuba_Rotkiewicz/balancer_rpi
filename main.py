""" 
TODO:
    1. Change rewards                                           done
    2. Change read angle freq                                   done
    3. Change learning rate                                     done
    4. Change valuing the rewards : iterations * rewards        done
    5. Add iterations to plot                                   done
    6. Gaussian reward function                                 done
    7. Run all iterations instead of untill fall                done
"""

import numpy as np
import sys
from moving_average_filter import MovingAverageFilter
from mpu6050 import MPU6050
from kalman import Kalman
import time
import traceback
from stepper import Stepper
from qlearning2d import QLearningEnv
import matplotlib.pyplot as plt
from reward_function import RewardFunction
import csv

EXP_NUM = 26

def calibrate_setpoint_angle(read_angle_period):
    """
    Function to calibrate initial setpoint angle. 
    The equilibrium point, so the balance state, is the angle = 0, 
    but physically it is impossible to mount IMU with exactly angle value = 0.
    That's why the initial angle is measured, called offset_angle, 
    and this particular angle is later treated as equilibrium point.
    After turning on the user should hold the robot vertically, within equilibrium point,
    for the time of calibration.
    Here the offset angle is not measured every time, it was measured once and it is hard coded below. 
    """
    offset_angle_array = []
    print("Calibration...")
    while len(offset_angle_array) < 100:
        pitch = mpu.get_pitch_angle()
        offset_angle_array = np.append(offset_angle_array, pitch)
        time.sleep(read_angle_period)

    offset_angle = sum(offset_angle_array[-50:])/50
    offset_angle_array = np.append(offset_angle_array, offset_angle)
    print('Offset Angle: ',offset_angle)
    return offset_angle

def get_angle(angle_dt):
    """
    Function performs actions to get final angle:
    - read the accelerometer and calculate the pitch angle
    - read the gyroscope and calculate the angular velocity
    - filter the pitch with moving average filter
    - run kalman filter to merge pitch and angular velocity 
      into stable and precise angle value in degrees
    """
    #angle_timer = time.time()
    pitch = mpu.get_pitch_angle()
    #pitch_dt = time.time() - angle_timer
    angular_velocity = -mpu.get_gyro_y_data()
    #ang_vel_dt = time.time() - pitch_dt - angle_timer
    #pitch_filtered = f.moving_average_filter_conv(pitch)
    #filt_dt = time.time() - ang_vel_dt - pitch_dt - angle_timer
    #angle = (1 - alpha) * (angle + angular_velocity * angle_dt)+ (alpha) * (pitch_filtered);       
    angle = k.getAngle(pitch, angular_velocity, angle_dt)
    #angle_dt = time.time() - filt_dt- ang_vel_dt - pitch_dt - angle_timer
    #print(f'Angle: {(pitch):9.4f}, pitch_filtered: {(pitch_filtered):9.4f}, Time: {(angle_dt):9.4f}, Kalman: {(angle):9.4f}')
    #print(f'p_dt:{pitch_dt:5.4f},a_v_dt:{ang_vel_dt:5.4f},f_dt:{filt_dt:5.4f},a_dt:{angle_dt:5.4f}')
    return angle, angular_velocity

READ_ANGLE_FREQUENCY = 100
READ_ANGLE_PERIOD = round(1/READ_ANGLE_FREQUENCY,3)

""" Initialize parameters """
angle = 0
pitch = 0
angular_velocity = 0
pitch_filtered = 0
offset_angle = 0
calibration_time_counter = 0
last_best_reward = 0
state_error = 0
last_state_error = 0

""" Initialize MPU6050 """
mpu = MPU6050()

""" Initial setpoint angle calibration """
offset_angle = 1.647081468459893
#2.179317260524555
#2.3845883137902093
# calibrate_setpoint_angle(READ_ANGLE_PERIOD)

""" Initialize pitch moving average filter """
f = MovingAverageFilter(22) 

""" Initialize Kalman filter - get initial angle from measuring pitch """
angle_init = mpu.get_pitch_angle()
k = Kalman(0.001, 0.003, 0.2, angle_init)

""" Initialize stepper motors """
sm = Stepper()

""" Initialize Q-Learning Environment """
q = QLearningEnv(learning_rate=0.1, discount=0.999, episodes=2000, epsilon=0.0,
    setpoint_angle=offset_angle, angle_range=5, discrete_angles=21, 
    action_range=2400, discrete_actions=3, show_every=20, iterations=500, 
    max_error=2,q_table_read="Data/q_table25last.txt") # ,q_table_read="Data/q_table8.txt"

""" Initialize reward function class """
r = RewardFunction(q.angle_range, q.discrete_angles)

""" End of initialization - save parameters to file and start timer """
q.save_params(str(EXP_NUM))
angle_timer = time.time()

""" Save aggr_ep_rewards to CSV file """
csv_columns = "ep,avg,min,max\n"
csv_file = "Data/aggr_ep_rewards" + str(EXP_NUM) + ".csv"
with open(csv_file, 'w') as file:
    file.write(csv_columns)

""" Start learning routine """
for episode in range(q.episodes):

    """ Stop and disable motors """
    sm.right_wheel_freq(0)
    sm.left_wheel_freq(0)
    sm.left_wheel_disable()
    sm.right_wheel_disable()

    """ The robot is initially leaning on the base, deflected by an angle, 
        Wait in a loop until the user puts it in upright position 
        It shows the actual angle for convenience """
    while (angle < q.setpoint_angle - q.discrete_angle_state_size or angle > q.setpoint_angle + q.discrete_angle_state_size):
        angle_dt = time.time() - angle_timer
        if(angle_dt > READ_ANGLE_PERIOD):
            angle_timer = time.time()
            angle, _= get_angle(angle_dt)
            print(f"Angle: {angle:9.4f}")

    """ When in upright position it is ready to start episode 
        Wait in a loop for the state to change by 1 
        Upright position is within (-1,1), start when outside of this range """
    print("Ready to start episode")
    while (angle > q.setpoint_angle - q.discrete_angle_state_size and angle < q.setpoint_angle + q.discrete_angle_state_size):
        angle_dt = time.time() - angle_timer
        if(angle_dt > READ_ANGLE_PERIOD):
            angle_timer = time.time()
            angle, _= get_angle(angle_dt)
            print(f"Angle: {angle:9.4f}")

    print("Episode started!")   

    sm.right_wheel_enable()
    sm.left_wheel_enable()

    episode_reward = 0

    discrete_state = q.get_discrete_state(angle)
    if(discrete_state > q.target_state):
        last_state_error = discrete_state - q.target_state
    else: 
        last_state_error = q.target_state - discrete_state

    done = False
    iterations_counter = 0
    reward = 0

    while not done:
        iterations_counter += 1

        """ Epsilon-greedy exploration vs exploitation """
        if np.random.random() > q.epsilon:
            action = np.argmax(q.q_table[discrete_state])
        else: 
            action = np.random.randint(0, q.discrete_actions)

        """ Take an action, so put proper frequency on the motors """
        freq = abs(q.actions[action])
        if q.actions[action] > 0:
            sm.right_wheel_forward()
            sm.left_wheel_forward()
        else:
            sm.right_wheel_backward()
            sm.left_wheel_backward()
        sm.right_wheel_freq(freq)
        sm.left_wheel_freq(freq)

        """ Read the angle """
        """ The period between iterations is defined by READ_ANGLE_PERIOD """
        while (angle_dt < READ_ANGLE_PERIOD):
            angle_dt = time.time() - angle_timer
        angle, _ = get_angle(angle_dt)
        angle_timer = time.time()
        
        """ Get resulting new state """
        new_discrete_state = q.get_discrete_state(angle)
        
        """ Set proper reward """
        if(new_discrete_state >= q.target_state):
            state_error = new_discrete_state - q.target_state
        else: 
            state_error = q.target_state - new_discrete_state

        """ If error is the same it will keep previous reward """
        #reward, last_state_error = r.direction_function(state_error, last_state_error, reward)
        """ Gaussian reward function """
        reward = r.gaussian_function(state_error)
        """ Simple evaluation: state_error < max_error -> 1 else -1 """
        #reward = r.simple_function(state_error, q.max_error)
        
        print(f"Episode: {episode},I: {iterations_counter}, Last reward: {reward:9.2f}, State error: {state_error}, Angle: {angle:9.4f}, angle_dt: {angle_dt:9.4f}, freq: {freq}")
        
        """ Evaluate state transition by calculating the q-function """
        if not done:
            max_future_q = np.max(q.q_table[new_discrete_state])
            current_q = q.q_table[discrete_state, action]
            new_q = (1 - q.learning_rate) * current_q + q.learning_rate * (reward + q.discount * max_future_q)
            q.q_table[discrete_state, action] = new_q

        """ End episode after q.iterations or when fell """
        #if (iterations_counter >= q.iterations): #or 
        #    done = True

        if (iterations_counter >= q.iterations or angle < q.angle_min-2 or angle > q.angle_max+2):
            done = True
            #sm.right_wheel_freq(0)
            #sm.left_wheel_freq(0)
            #sm.left_wheel_disable()
            #sm.right_wheel_disable()

        episode_reward += reward

        discrete_state = new_discrete_state

    """ Epsilon-greedy exploration - decay epsilon """
    #if (q.end_epsilon_decaying >= episode >= q.start_epsilon_decaying):
    #    q.epsilon -= q.epsilon_decay_value

    """ Decay learning rate """
    #if (q.end_learning_rate_decaying >= episode >= q.start_learning_rate_decaying):
    #    q.learning_rate -= q.learning_rate_decay_value

    q.ep_rewards.append(episode_reward)
    q.ep_iterations.append(iterations_counter)

    """ Save the best q-table """
    if (episode_reward * iterations_counter > last_best_reward):
        last_best_reward = episode_reward * iterations_counter
        q.save_qtable(str(EXP_NUM)+'best')

    #if (episode_reward > last_best_reward):
    #    last_best_reward = episode_reward
    #    q.save_qtable(str(EXP_NUM)+'best')

    """ Save evaluation of every 'q.show_every' episode """
    if not episode % q.show_every:
        average_reward = sum(q.ep_rewards[-q.show_every:])/len(q.ep_rewards[-q.show_every:])
        average_iterations = sum(q.ep_iterations[-q.show_every:])/len(q.ep_iterations[-q.show_every:])
        q.aggr_ep_rewards['ep'].append(episode)
        q.aggr_ep_rewards['avg'].append(average_reward)
        q.aggr_ep_rewards['min'].append(min(q.ep_rewards[-q.show_every:]))
        q.aggr_ep_rewards['max'].append(max(q.ep_rewards[-q.show_every:]))
        q.aggr_ep_rewards['iter'].append(average_iterations)

        q.save_qtable(str(EXP_NUM)+'last')
        
        with open(csv_file, 'a') as file:
            file.write(f"{q.aggr_ep_rewards['ep'][-1]},{q.aggr_ep_rewards['avg'][-1]},{q.aggr_ep_rewards['min'][-1]},{q.aggr_ep_rewards['max'][-1]}\n")
        
        print(f"Episode: {episode} avg: {average_reward} min: {min(q.ep_rewards[-q.show_every:])} max {max(q.ep_rewards[-q.show_every:])}") # iter {average_iterations}
        
""" Stop and disable motors """
sm.right_wheel_freq(0)
sm.left_wheel_freq(0)
sm.left_wheel_disable()
sm.right_wheel_disable()

""" Plot and save .png of the final results """
plt.plot(q.aggr_ep_rewards['ep'], q.aggr_ep_rewards['avg'], label="avg")
plt.plot(q.aggr_ep_rewards['ep'], q.aggr_ep_rewards['min'], label="min")
plt.plot(q.aggr_ep_rewards['ep'], q.aggr_ep_rewards['max'], label="max")
plt.plot(q.aggr_ep_rewards['ep'], q.aggr_ep_rewards['iter'], label="iter")
plt.grid()
plt.title("Rewards in episodes")
plt.xlabel("Episode")
plt.ylabel("Reward")
plt.legend(loc=4)
plt.savefig("Data/fig" + str(EXP_NUM) + ".png")
plt.show()

sys.exit()

