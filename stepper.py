from time import sleep 
import pigpio
import Encoder
import numpy as np
import sys

class Stepper():
    """
    Default pinout: 

    STEPPER_MOTOR_DIR_RIGHT = 6
    STEPPER_MOTOR_STEP_RIGHT = 12
    STEPPER_MOTOR_EN_RIGHT = 5

    STEPPER_MOTOR_DIR_LEFT = 16
    STEPPER_MOTOR_STEP_LEFT = 13
    STEPPER_MOTOR_EN_LEFT = 26

    ENC_LEFT_A = 22
    ENC_LEFT_B = 23

    ENC_RIGHT_A = 24
    ENC_RIGHT_B = 25
    """

    WHEEL_DIAMETER_CM = 8.1
    ENCODER_RESOLUTION = 20

    def __init__(self, STEPPER_MOTOR_DIR_RIGHT = 6, STEPPER_MOTOR_STEP_RIGHT = 12, 
        STEPPER_MOTOR_EN_RIGHT = 5, STEPPER_MOTOR_DIR_LEFT = 16,
        STEPPER_MOTOR_STEP_LEFT = 13, STEPPER_MOTOR_EN_LEFT = 26, ENC_LEFT_A = 25,
        ENC_LEFT_B = 24, ENC_RIGHT_A = 23, ENC_RIGHT_B = 22):

        self.stepper_motors = pigpio.pi()
        self.stepper_motor_dir_right = STEPPER_MOTOR_DIR_RIGHT
        self.stepper_motor_step_right = STEPPER_MOTOR_STEP_RIGHT
        self.stepper_motor_en_right = STEPPER_MOTOR_EN_RIGHT

        self.stepper_motor_dir_left = STEPPER_MOTOR_DIR_LEFT
        self.stepper_motor_step_left = STEPPER_MOTOR_STEP_LEFT
        self.stepper_motor_en_left = STEPPER_MOTOR_EN_LEFT

        self.left_wheel_forward()
        self.right_wheel_forward()
        self.left_wheel_disable()
        self.right_wheel_disable()
        self.right_wheel_freq(0)
        self.left_wheel_freq(0)

        self.encoder_constant = self.ENCODER_RESOLUTION * np.pi * self.WHEEL_DIAMETER_CM / 360

        self.encoder_left = Encoder.Encoder(ENC_LEFT_A,ENC_LEFT_B)
        self.encoder_right = Encoder.Encoder(ENC_RIGHT_B, ENC_RIGHT_A)
        
    def left_wheel_forward(self):
        self.stepper_motors.write(self.stepper_motor_dir_left, 1)

    def right_wheel_forward(self):
        self.stepper_motors.write(self.stepper_motor_dir_right, 0)

    def left_wheel_backward(self):
        self.stepper_motors.write(self.stepper_motor_dir_left, 0)

    def right_wheel_backward(self):
        self.stepper_motors.write(self.stepper_motor_dir_right, 1)

    def left_wheel_enable(self):
        self.stepper_motors.write(self.stepper_motor_en_left, 0)

    def right_wheel_enable(self):
        self.stepper_motors.write(self.stepper_motor_en_right, 0)

    def left_wheel_disable(self):
        self.stepper_motors.write(self.stepper_motor_en_left, 1)

    def right_wheel_disable(self):
        self.stepper_motors.write(self.stepper_motor_en_right, 1)

    def left_wheel_freq(self, freq):
        info = self.stepper_motors.hardware_PWM(self.stepper_motor_step_left,freq,500000)
        return info

    def right_wheel_freq(self, freq):
        info = self.stepper_motors.hardware_PWM(self.stepper_motor_step_right,freq,500000)
        return info

    def get_left_position(self):
        self.left_wheel_position = self.encoder_left.read() * self.encoder_constant
        return self.left_wheel_position
    
    def get_right_position(self):
        self.right_wheel_position = self.encoder_right.read() * self.encoder_constant
        return self.right_wheel_position

if __name__ == "__main__":

    sm = Stepper()
    i = 0
    """
    sm.left_wheel_backward()
    sm.right_wheel_backward()
    sm.left_wheel_enable()
    sm.right_wheel_enable()
    freq = 0
    while i < 100000:
        freq = freq + 500
        sm.right_wheel_freq(freq)
        sm.left_wheel_freq(freq)
        if(freq >= 4000):
            sm.right_wheel_freq(0)
            sm.left_wheel_freq(0)
            break
        sleep(0.050)
    sleep(1)
    sm.left_wheel_disable()
    sm.right_wheel_disable()
    """ 
    #CHECK STEPPER AND ENCODER
    """
    sm.left_wheel_forward()
    sm.right_wheel_forward()
    sm.left_wheel_enable()
    sm.right_wheel_enable()
    
    for i in range(0,1000):
        #print(f"Info right: {sm.right_wheel_freq(i*8)}, info left: {sm.left_wheel_freq(i*8)}")
        #print(f"f_left: {i*6}, f_right: {i*8}, left_pos: {sm.get_left_position()}, right_pos: {sm.get_right_position()}")
        sm.right_wheel_freq(i*100)
        sm.left_wheel_freq(i*100)
        print(f"Freq: {i*100}")
        sleep(1)
        sm.right_wheel_freq(0)
        sm.left_wheel_freq(0)
        sleep(0.1)
    """
    sm.right_wheel_freq(0)
    sm.left_wheel_freq(0)
    sm.left_wheel_disable()
    sm.right_wheel_disable()
    
    # CHECK ENCODER ONLY
    
    while True:
        #i = i + 1
        print(f"left_pos: {sm.get_left_position()}, right_pos: {sm.get_right_position()}")
    
    
    sys.exit()