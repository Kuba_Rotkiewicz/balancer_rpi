import numpy as np
import matplotlib.pyplot as plt

class QLearningEnv():
    """
        Q Learning environment class

        Attributes
        ----------
        learning_rate : int
            Learning rate influences to what extent newly acquired information overrides old information
        discount = int
            Discount is a number between 0 and 1. It has the effect of valuing 
            rewards received earlier than those received later
        epsilon = int
            Epsilon is a decay value; sometimes the agent can take random action 
            and sometimes the one with the greatest q-value
            In time this value should decay, and therefore more and more max(q-values) 
            are taken rather than random actions
        episodes : int
            Number of episodes in learning

        setpoint_angle : float
            Angle at which the robot is standing vertically and stable
        angle_range : int
            Angle values that are considered to be in the observation space. 
            If angle_range = 10 (default), the observation space will be:
            (setpoint_angle - angle_range : setpoint_angle + angle_range)
        discrete_angles : int
            Angle range discretization. How many discrete angle values will be in the observation space

        setpoint_angular_velocity : int
            Angular_velocity at which the robot is standing vertically and stable
        angular_velocity_range : int 
            Angular velocity values that are considered to be in the observation space. 
        discrete_angular_velocities : int
            Angular velocity range discretization. How many discrete angle values will be in the observation space
        
        action_range : int
            Action values (in this case stepper motor control frequency). 
            If action_range = 28000 (default), the action space will be (-action_range : action_range)
        discrete_actions : int
            Action range discretization. How many discrete action values will be in the action space

        show_every : int
            Each show_every episode rewards will be calculated and stored
        """
    def __init__(self, learning_rate = 0.7, discount = 0.95, episodes = 20000, epsilon = 0.6,
        setpoint_angle = 0, angle_range = 10, discrete_angles = 21, action_range = 28000, 
        discrete_actions = 21, show_every = 500, iterations = 200, q_table_read = 0, max_error = 2):

        self.discount = discount
        self.episodes = episodes

        self.learning_rate = learning_rate
        self.start_learning_rate_decaying = 1
        self.end_learning_rate_decaying = self.episodes // 2    # "// 2" -> for half of the episodes the agent may take random actions to learn
        self.learning_rate_decay_value = (learning_rate-0.1)/(self.end_learning_rate_decaying - self.start_learning_rate_decaying)
        print(self.learning_rate_decay_value)
        self.epsilon = epsilon
        self.start_epsilon_decaying = 1
        # "// 2" -> for half of the episodes the agent may take random actions to learn
        self.end_epsilon_decaying = self.episodes // 2        
        self.epsilon_decay_value = self.epsilon/(self.end_epsilon_decaying - self.start_epsilon_decaying)

        # Angle part
        self.setpoint_angle = setpoint_angle
        self.angle_range = angle_range
        self.angle_min = setpoint_angle - angle_range
        self.angle_max = setpoint_angle + angle_range
        self.discrete_angles = discrete_angles
        self.angle_state = np.linspace(self.angle_min, self.angle_max, self.discrete_angles) #, dtype=int
        print("Angle states: ", self.angle_state)
        self.discrete_angle_state_size = (self.angle_max - self.angle_min) / (self.discrete_angles - 1)
        print("Discrete_state_size: ", self.discrete_angle_state_size)
        
        self.target_state = self.get_discrete_state(self.setpoint_angle)
        print(f"Target state: {self.target_state}")

        self.max_error = max_error

        # Action part
        self.action_min = -action_range
        self.action_max = action_range
        self.discrete_actions = discrete_actions
        #self.actions = np.linspace(self.action_min, self.action_max, self.discrete_actions, dtype=int)
        #self.actions = self.quadraticspace(self.action_max)
        self.actions = self.linearspace(-(self.discrete_actions-1)/2, 
        (self.discrete_actions-1)/2, self.action_min, self.action_max, self.discrete_actions)
        print("Actions: ", self.actions)

        #self.q_table = np.zeros((len(self.angle_state), len(self.actions)))
        if (q_table_read != 0):
            self.q_table = self.load_qtable(q_table_read)
        else:
            self.q_table = np.zeros((len(self.angle_state), len(self.actions)))
        print("Q-Table: ", np.shape(self.q_table))
        print(self.q_table)

        self.iterations = iterations
        self.ep_iterations = []

        self.ep_rewards = []
        self.aggr_ep_rewards = {'ep': [], 'avg': [], 'min': [], 'max': [], 'iter': []}
    
        self.show_every = show_every
        
    def linearspace(self, x_min, x_max, y_min, y_max, count):
        a = np.zeros(count).astype(int)
        middle = (count-1)/2
        for i in range(0, count):
            x = i-middle
            a[i] = round((y_min + ((y_max-y_min)/(x_max-x_min))*(x-x_min))/10)*10
        return a

    def quadraticspace(self, max):
        a = np.zeros(self.discrete_actions).astype(int)
        middle = (self.discrete_actions-1)/2
        for i in range(0, self.discrete_actions):
            if i < middle:
                a[i] = round((-pow((i - middle), 2) * max/100)/10) * 10
            elif i == middle:
                a[i] = 0
            else:
                a[i] = round((pow((i - middle), 2) * max/100)/10) * 10
        return a

    def get_discrete_state(self, angle):
        discrete_angle_state = (angle - self.angle_min) * (self.discrete_angles-1) / (self.angle_max - self.angle_min)
        #print(f"d_ang: {discrete_angle_state}")

        if (discrete_angle_state >= self.discrete_angles-1):
            discrete_angle_state = self.discrete_angles - 1
        elif (discrete_angle_state < 0):
            discrete_angle_state = 0

        return int(round(discrete_angle_state))

    def save_qtable(self, number):
        with open("Data/q_table" + number + ".txt", 'w') as outfile:
            outfile.write('# Array shape: {0}\n'.format(self.q_table.shape))
            np.savetxt(outfile, self.q_table, fmt='%-7f')

    def load_qtable(self,filename):
            data = np.loadtxt(filename)
            return data

    def save_params(self, number):
        with open("Data/params" + number + ".txt", 'w') as outfile:
            outfile.write('# Learning rate: {0}\n'.format(self.learning_rate))
            outfile.write('# Discount: {0}\n'.format(self.discount))
            outfile.write('# Episodes: {0}\n'.format(self.episodes))
            outfile.write('# Epsilon: {0}\n'.format(self.epsilon))
            outfile.write('# Iterations: {0}\n'.format(self.iterations))
            outfile.write('# Setpoint angle: {0}\n'.format(self.setpoint_angle))
            outfile.write('# Angle min: {0}\n'.format(self.angle_min))
            outfile.write('# Angle max: {0}\n'.format(self.angle_max))
            outfile.write('# Discrete angles: {0}\n'.format(self.discrete_angles))
            outfile.write('# Action min: {0}\n'.format(self.action_min))
            outfile.write('# Action max: {0}\n'.format(self.action_max))
            outfile.write('# Discrete actions: {0}\n'.format(self.discrete_actions))
            outfile.write('# Show every: {0}\n'.format(self.show_every))
            outfile.write('# Max error: {0}\n'.format(self.max_error))
            outfile.write('# Q_table shape: {0}\n'.format(self.q_table.shape))
            outfile.write('# Init Q_table: \n')
            np.savetxt(outfile, self.q_table, fmt='%-7f')


if __name__ == "__main__":
    # Test variables
    angle = -4.45

    q = QLearningEnv(setpoint_angle=0, episodes=2600, angle_range=5,discrete_angles=21,
    action_range=3200, discrete_actions=3, show_every=10)
    discrete_state = q.get_discrete_state(angle)
    print(q.discrete_angle_state_size)
    print(q.angle_state[discrete_state])
    print(q.actions)
    #print(q.action_min)
    #q.save_qtable(str(11))
    #q.q_table = q.load_qtable("Data/q_table10.txt")
    #q.save_params(str(11))
    #print(0.6 - q.epsilon_decay_value * 400)
    #print(0.8 - q.learning_rate_decay_value * 400)

    """
    for episode in range(q.episodes):

        while (angle < q.setpoint_angle - q.discrete_angle_state_size or angle > q.setpoint_angle + q.discrete_angle_state_size):
            #time.sleep(0.050)
            angle = 3#get_angle()
            print(f"Angle: {angle:9.4f}")
        print("Ready to start episode")

        while (angle > q.setpoint_angle - q.discrete_angle_state_size and angle < q.setpoint_angle + q.discrete_angle_state_size):
            # time.sleep(0.050)
            angle = 5   #get_angle()
            # angular_velocity = get angular velocity
            print(f"Angle: {angle:9.4f}")
        print("Episode started!")

        episode_reward = 0

        discrete_state = q.get_discrete_state(angle)

        done = False

        while not done:

            if np.random.random() > q.epsilon:
                action = np.argmax(q.q_table[discrete_state])
            else: 
                action = np.random.randint(0, q.discrete_actions)

            angle, angular_velocity, reward, done, _ = q.step(action)
            episode_reward += reward
            new_discrete_state = q.get_discrete_state(angle)
            if not done:
                max_future_q = np.max(q.q_table[new_discrete_state])
                current_q = q.q_table[discrete_state + (action, )]
                new_q = (1 - q.learning_rate) * current_q + q.learning_rate * (reward + q.discount * max_future_q)
                q.q_table[discrete_state + (action, )] = new_q
            elif (angle > q.setpoint_angle - q.discrete_angle_state_size and angle < q.setpoint_angle + q.discrete_angle_state_size):
                print(f"We made it on espisode: {episode}")
                q.q_table[discrete_state + (action, )] = 0

            discrete_state = new_discrete_state
        
        if q.end_epsilon_decaying >= episode >= q.start_epsilon_decaying:
            q.epsilon -= q.epsilon_decay_value

        q.ep_rewards.append(episode_reward)

        if not episode % q.SHOW_EVERY:
            average_reward = sum(q.ep_rewards[-q.show_every:])/len(q.ep_rewards[-q.show_every:])
            q.aggr_ep_rewards['ep'].append(episode)
            q.aggr_ep_rewards['avg'].append(average_reward)
            q.aggr_ep_rewards['min'].append(min(q.ep_rewards[-q.show_every:]))
            q.aggr_ep_rewards['max'].append(max(q.ep_rewards[-q.show_every:]))

            print(f"Episode: {episode} avg: {average_reward} min: {min(q.ep_rewards[-q.show_every:])} max {max(q.ep_rewards[-q.show_every:])}")
    
    plt.plot(q.aggr_ep_rewards['ep'], q.aggr_ep_rewards['avg'], label="avg")
    plt.plot(q.aggr_ep_rewards['ep'], q.aggr_ep_rewards['min'], label="min")
    plt.plot(q.aggr_ep_rewards['ep'], q.aggr_ep_rewards['max'], label="max")
    plt.legend(loc=4)
    plt.show()
    """