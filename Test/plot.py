import matplotlib.pyplot as plt
import matplotlib.animation as animation
from mpu6050 import MPU6050
import time
import threading
import numpy as np
from kalman import Kalman
from moving_average_filter import MovingAverageFilter
import csv

def handler():
    global pitch
    global ang_vel
    global filtered
    global angle
    global complementary
    global angle_fil
    
    SAVE_TO_CSV = 1
    if SAVE_TO_CSV:
        with open('Data/filters1.csv','w') as fd:
            #fd.write(f"{pitch},{filtered},{angle_dt},{angle}\n")
            fd.write("Pitch,FilteredPitch,Kalman,Complementary,Gyro,dt\n")

    mpu = MPU6050()
    k = Kalman(0.001, 0.003, 0.2,mpu.get_pitch_angle())
    k1 = Kalman(0.001, 0.003, 0.2,mpu.get_pitch_angle())
    f = MovingAverageFilter(22) 
    start = time.time()
    alpha = 0.02
    while True:
        pitch = mpu.get_pitch_angle()
        ang_vel = -mpu.get_gyro_y_data()
        filtered = f.moving_average_filter_conv(pitch)
        angle_dt = time.time() - start
        complementary = (1 - alpha) * (angle + ang_vel * angle_dt)+ (alpha) * (pitch)
        angle = k.getAngle(pitch, ang_vel, angle_dt)
        angle_fil = k1.getAngle(filtered, ang_vel, angle_dt)
        start = time.time()
        print(f"P:{(pitch):9.4f}, V:{(ang_vel):9.4f}, F: {(filtered):9.4f}, A: {(angle):9.4f}, A_r: {(angle_fil):9.4f},C: {(complementary):9.4f}, A_dt: {(angle_dt):9.4f}")
        time.sleep(0.020)
        if SAVE_TO_CSV:
            with open('Data/filters1.csv','a') as fd:
                #fd.write(f"{pitch},{filtered},{angle_dt},{angle}\n")
                fd.write(f"{pitch},{filtered},{angle},{complementary},{ang_vel},{angle_dt}\n")

global pitch
pitch = 0
global ang_vel
ang_vel = 0
global filtered
filtered = 0
global angle
angle = 0
global complementary
complementary = 0
global angle_fil
angle_fil = 0

# Parameters
x_len = 200         # Number of points to display
y_range = [-40, 40]  # Range of possible Y values to display

# Create figure for plotting
fig = plt.figure()
global number_of_lines
number_of_lines = 4
ax = [[0] for i in range(number_of_lines)]
xs = [[0] for i in range(number_of_lines)]
ys = [[0] for i in range(number_of_lines)]
line = [[0] for i in range(number_of_lines)]

for i in range(number_of_lines):
    ax[i] = fig.add_subplot(1, 1, 1)
    xs[i] = list(range(0, 200))
    ys[i] = [0] * x_len
    ax[i].set_ylim(y_range)
    # Create a blank line. We will update the line in animate
line[0], = ax[i].plot(xs[i], ys[i],c='r', label='line 1', linewidth=1, markersize=1)
line[1], = ax[i].plot(xs[i], ys[i],c='g', label='line 2', linewidth=1, markersize=1)
line[2], = ax[i].plot(xs[i], ys[i],c='b', label='line 3', linewidth=1, markersize=1)
line[3], = ax[i].plot(xs[i], ys[i],c='k', label='line 4', linewidth=1, markersize=1)
    #c=np.random.rand(3,) np.array([0.3, 0.8, 0.5])
# This function is called periodically from FuncAnimation
def animate(i, ys):

    # Read temperature (Celsius) from TMP102
    y1 = round(angle, 2)
    y2 = round(complementary, 2)
    y3 = round(ang_vel, 2)
    y4 = round(pitch, 2)

    # Add y to list
    ys[0].append(y1)
    ys[1].append(y2)
    ys[2].append(y3)
    ys[3].append(y4)

    for j in range(number_of_lines):
        ys[j] = ys[j][-x_len:]
        line[j].set_ydata(ys[j])
    # Limit y list to set number of items
    #ys[0] = ys[0][-x_len:]
    #ys[1] = ys[1][-x_len:]
    #ys[2] = ys[2][-x_len:]
    #ys[3] = ys[3][-x_len:]
    # Update line with new Y values
    #line[0].set_ydata(ys[0])
    #line[1].set_ydata(ys[1])
    #line[2].set_ydata(ys[2])
    #line[3].set_ydata(ys[3])
    # Add labels
    
    return line

# Start measurements
t = threading.Thread(target=handler)
t.start()

# Set up plot to call animate() function periodically
ani = animation.FuncAnimation(fig,
    animate,
    fargs=(ys,),
    interval=10,
    blit=True)
plt.show()