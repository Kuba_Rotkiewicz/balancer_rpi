import numpy as np
import sys
from moving_average_filter import MovingAverageFilter
from mpu6050 import MPU6050
from kalman import Kalman
#import csv
import time
import traceback
from stepper import Stepper
from qlearning import QLearningEnv
import matplotlib.pyplot as plt

def calibrate_setpoint_angle(read_angle_period):
    offset_angle_array = []
    print("Calibration...")
    while len(offset_angle_array) < 100:
        pitch = mpu.get_pitch_angle()
        offset_angle_array = np.append(offset_angle_array, pitch)
        time.sleep(read_angle_period)

    offset_angle = sum(offset_angle_array[-50:])/50
    offset_angle_array = np.append(offset_angle_array, offset_angle)
    print('Offset Angle: ',offset_angle)
    return offset_angle

def get_angle(angle_dt):
    #angle_timer = time.time()
    pitch = mpu.get_pitch_angle()
    #pitch_dt = time.time() - angle_timer
    angular_velocity = -mpu.get_gyro_y_data()
    #ang_vel_dt = time.time() - pitch_dt - angle_timer
    pitch_filtered = f.moving_average_filter_conv(pitch)
    #filt_dt = time.time() - ang_vel_dt - pitch_dt - angle_timer
    #angle = (1 - alpha) * (angle + angular_velocity * angle_dt)+ (alpha) * (pitch_filtered);       
    angle = k.getAngle(pitch_filtered, angular_velocity, angle_dt)
    #angle_dt = time.time() - filt_dt- ang_vel_dt - pitch_dt - angle_timer
    #print(f'Angle: {(pitch):9.4f}, pitch_filtered: {(pitch_filtered):9.4f}, Time: {(angle_dt):9.4f}, Kalman: {(angle):9.4f}')
    #print(f'p_dt:{pitch_dt:5.4f},a_v_dt:{ang_vel_dt:5.4f},f_dt:{filt_dt:5.4f},a_dt:{angle_dt:5.4f}')
    return angle, angular_velocity

READ_ANGLE_FREQUENCY = 100
READ_ANGLE_PERIOD = round(1/READ_ANGLE_FREQUENCY,3)
CONTROL_FREQUENCY_RESOLUTION = 50 # Control signal discretized to (0,50,100,...)

angle = 0
pitch = 0
angular_velocity = 0
pitch_filtered = 0

offset_angle = 0
fall_trigger = 1
calibration_time_counter = 0

left_freq_old = 0
right_freq_old = 0

left_wheel_position = 0
right_wheel_position = 0

# Initialize MPU6050
mpu = MPU6050()
# Initial setpoint angle calibration
offset_angle = 2.3845883137902093
# calibrate_setpoint_angle(READ_ANGLE_PERIOD)
# Initialize pitch angle moving average filter
f = MovingAverageFilter(22) 
# Initialize Kalman filter
angle_init = mpu.get_pitch_angle()
k = Kalman(0.001, 0.003, 0.2, angle_init)
# Initialize stepper motors
sm = Stepper()
# Initialize Q-Learning Environment
q = QLearningEnv(setpoint_angle=offset_angle, action_range=18000 ,discrete_angular_velocities = 7, 
    discount= 0.95, learning_rate=0.7, episodes=10, show_every=2)
#discrete_state = q.get_discrete_state(angle, angular_velocity)
#print(discrete_state)
#print(q.angle_state[discrete_state[0]])
#print(q.angular_velocity_state[discrete_state[1]])
#print(q.q_table[discrete_state])

angle_timer = time.time()
"""
while True:
    angle_dt = time.time()-angle_timer
    if(angle_dt > READ_ANGLE_PERIOD):
        angle_timer = time.time()

        angle, _ = get_angle(angle_dt)

        if calibration_time_counter < 20:
            calibration_time_counter += 1
            if calibration_time_counter == 20:
                break
"""
for episode in range(q.episodes):

    sm.right_wheel_freq(0)
    sm.left_wheel_freq(0)
    sm.left_wheel_disable()
    sm.right_wheel_disable()

    while (angle < q.setpoint_angle - q.discrete_angle_state_size or angle > q.setpoint_angle + q.discrete_angle_state_size):
        angle_dt = time.time() - angle_timer
        if(angle_dt > READ_ANGLE_PERIOD):
            angle_timer = time.time()
            angle, _ = get_angle(angle_dt)
            print(f"Angle: {angle:9.4f}")

    print("Ready to start episode")

    while (angle > q.setpoint_angle - q.discrete_angle_state_size and angle < q.setpoint_angle + q.discrete_angle_state_size):
        angle_dt = time.time() - angle_timer
        if(angle_dt > READ_ANGLE_PERIOD):
            angle_timer = time.time()
            angle, angular_velocity = get_angle(angle_dt)
            print(f"Angle: {angle:9.4f}")

    print("Episode started!")   

    sm.right_wheel_enable()
    sm.left_wheel_enable()

    episode_reward = 0

    discrete_state = q.get_discrete_state(angle, angular_velocity)
    if(discrete_state[0] > q.target_state[0]):
        last_state_error = discrete_state[0] - q.target_state[0]
    else: 
        last_state_error = q.target_state[0] - discrete_state[0]

    done = False
    iterations_counter = 0
    reward = 0

    while not done:
        iterations_counter += 1

        if np.random.random() > q.epsilon:
            action = np.argmax(q.q_table[discrete_state])
        else: 
            action = np.random.randint(0, q.discrete_actions)

        freq = abs(q.actions[action])
        if q.actions[action] > 0:
            sm.right_wheel_forward()
            sm.left_wheel_forward()
        else:
            sm.right_wheel_backward()
            sm.left_wheel_backward()
        sm.right_wheel_freq(freq)
        sm.left_wheel_freq(freq)

        while (angle_dt < READ_ANGLE_PERIOD):
            angle_dt = time.time() - angle_timer
        angle, angular_velocity = get_angle(angle_dt)
        angle_timer = time.time()
        print(f"Episode: {episode},I: {iterations_counter}, Last reward: {reward}, Angle: {angle:9.4f}, angle_dt: {angle_dt:9.4f}")

        new_discrete_state = q.get_discrete_state(angle, angular_velocity)
        if(new_discrete_state[0] >= q.target_state[0]):
            state_error = new_discrete_state[0] - q.target_state[0]
        else: 
            state_error = q.target_state[0] - new_discrete_state[0]

        if (new_discrete_state == q.target_state):
            reward = 100
            last_state_error = 0
        elif (state_error >= last_state_error):
            reward = -10
            last_state_error = state_error
        elif (state_error < last_state_error):
            reward = +1
            last_state_error = state_error

        if (iterations_counter >= q.iterations_in_episode or angle < q.angle_min-5 or angle > q.angle_max+5):
            done = True
        
        episode_reward += reward
        
        if not done:
            max_future_q = np.max(q.q_table[new_discrete_state])
            current_q = q.q_table[discrete_state + (action, )]
            new_q = (1 - q.learning_rate) * current_q + q.learning_rate * (reward + q.discount * max_future_q)
            q.q_table[discrete_state + (action, )] = new_q
        #elif (angle > q.setpoint_angle - q.discrete_angle_state_size and angle < q.setpoint_angle + q.discrete_angle_state_size):
        #    print(f"We made it on espisode: {episode}")
        #    q.q_table[discrete_state + (action, )] = 0

        discrete_state = new_discrete_state
    
    if (q.end_epsilon_decaying >= episode >= q.start_epsilon_decaying):
        q.epsilon -= q.epsilon_decay_value

    q.ep_rewards.append(episode_reward)

    if not episode % q.show_every:
        average_reward = sum(q.ep_rewards[-q.show_every:])/len(q.ep_rewards[-q.show_every:])
        q.aggr_ep_rewards['ep'].append(episode)
        q.aggr_ep_rewards['avg'].append(average_reward)
        q.aggr_ep_rewards['min'].append(min(q.ep_rewards[-q.show_every:]))
        q.aggr_ep_rewards['max'].append(max(q.ep_rewards[-q.show_every:]))

        print(f"Episode: {episode} avg: {average_reward} min: {min(q.ep_rewards[-q.show_every:])} max {max(q.ep_rewards[-q.show_every:])}")
        q.save_qtable("q_table.txt")

sm.right_wheel_freq(0)
sm.left_wheel_freq(0)
sm.left_wheel_disable()
sm.right_wheel_disable()

plt.plot(q.aggr_ep_rewards['ep'], q.aggr_ep_rewards['avg'], label="avg")
plt.plot(q.aggr_ep_rewards['ep'], q.aggr_ep_rewards['min'], label="min")
plt.plot(q.aggr_ep_rewards['ep'], q.aggr_ep_rewards['max'], label="max")
plt.legend(loc=4)
plt.show()

sys.exit()

