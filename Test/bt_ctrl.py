from evdev import InputDevice, categorize, ecodes, InputEvent
import sys

def main():
    device = InputDevice('/dev/input/event1')
    device.grab()

    for event in device.read_loop():
        if event == ecodes.EV_REL:
            print(categorize(event))

    device.ungrab()

if __name__ == '__main__':
    main()
"""
    try:
        main()
    except KeyboardInterrupt:
        device.ungrab()
        print('Interrupted')
    finally:
        sys.exit(0)
"""
"""
#import evdev
from evdev import InputDevice, categorize, ecodes, InputEvent

#creates object 'gamepad' to store the data
#you can call it whatever you like
gamepad = InputDevice('/dev/input/event1')

#prints out device info at start
print(gamepad)

#evdev takes care of polling the controller in a loop
for event in gamepad.read_loop():
    print(categorize(event))
"""