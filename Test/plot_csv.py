import numpy as np
import matplotlib.pyplot as plt
import csv

EXP_NUM = 25

row_count = 70

with open('Data/aggr_ep_rewards25.csv', 'r') as file:
    reader = csv.reader(file)
    next(reader)
    #print(row_count)
    ep = np.empty(row_count)
    avg = np.empty(row_count)
    min = np.empty(row_count)
    max = np.empty(row_count)
    counter = 0
    for row in reader:
        print(row[1])
        ep[counter] = row[0]
        avg[counter] = row[1]
        min[counter] = row[2]
        max[counter] = row[3]
        counter = counter + 1

""" Plot and save .png of the final results """
plt.plot(ep,avg, label="avg")
plt.plot(ep,min, label="min")
plt.plot(ep,max, label="max")
plt.grid()
plt.title("Rewards in episodes")
plt.xlabel("Episode")
plt.ylabel("Reward")
plt.legend(loc=4)
plt.savefig("Data/fig" + str(EXP_NUM) + ".png")
plt.show()