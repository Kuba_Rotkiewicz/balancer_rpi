import numpy as np
import matplotlib.pyplot as plt

class QLearningEnv():
    """
        Q Learning environment class

        Attributes
        ----------
        learning_rate : int
            Learning rate influences to what extent newly acquired information overrides old information
        discount = int
            Discount is a number between 0 and 1. It has the effect of valuing 
            rewards received earlier than those received later
        epsilon = int
            Epsilon is a decay value; sometimes the agent can take random action 
            and sometimes the one with the greatest q-value
            In time this value should decay, and therefore more and more max(q-values) 
            are taken rather than random actions
        episodes : int
            Number of episodes in learning

        setpoint_angle : float
            Angle at which the robot is standing vertically and stable
        angle_range : int
            Angle values that are considered to be in the observation space. 
            If angle_range = 10 (default), the observation space will be:
            (setpoint_angle - angle_range : setpoint_angle + angle_range)
        discrete_angles : int
            Angle range discretization. How many discrete angle values will be in the observation space

        setpoint_angular_velocity : int
            Angular_velocity at which the robot is standing vertically and stable
        angular_velocity_range : int 
            Angular velocity values that are considered to be in the observation space. 
        discrete_angular_velocities : int
            Angular velocity range discretization. How many discrete angle values will be in the observation space
        
        action_range : int
            Action values (in this case stepper motor control frequency). 
            If action_range = 28000 (default), the action space will be (-action_range : action_range)
        discrete_actions : int
            Action range discretization. How many discrete action values will be in the action space

        show_every : int
            Each show_every episode rewards will be calculated and stored
        """
    def __init__(self, learning_rate = 0.1, discount = 0.95, episodes = 20000, epsilon = 0.6,
        setpoint_angle = 0, angle_range = 10, discrete_angles = 21, setpoint_angular_velocity = 0, 
        angular_velocity_range = 250, discrete_angular_velocities = 21, action_range = 28000, 
        discrete_actions = 21, show_every = 500, iterations_in_episode = 200):

        self.learning_rate = learning_rate
        self.discount = discount
        self.episodes = episodes
        self.iterations_in_episode = iterations_in_episode

        self.epsilon = epsilon
        self.start_epsilon_decaying = 1
        # "// 2" -> for half of the episodes the agent may take random actions to learn
        self.end_epsilon_decaying = self.episodes // 2        
        self.epsilon_decay_value = self.epsilon/(self.end_epsilon_decaying - self.start_epsilon_decaying)

        # Angle part
        self.setpoint_angle = setpoint_angle
        self.angle_min = setpoint_angle - angle_range
        self.angle_max = setpoint_angle + angle_range
        self.discrete_angles = discrete_angles
        self.angle_state = np.linspace(self.angle_min, 
            self.angle_max, self.discrete_angles) #, dtype=int
        print("Angle states: ", self.angle_state)
        self.discrete_angle_state_size = int((self.angle_max - self.angle_min) / (self.discrete_angles - 1))
        print("Discrete_state_size: ", self.discrete_angle_state_size)
        
        # Angular velocity part
        self.setpoint_angular_velocity = setpoint_angular_velocity 
        self.angular_velocity_min = -angular_velocity_range
        self.angular_velocity_max = angular_velocity_range
        self.discrete_angular_velocities = discrete_angular_velocities
        self.angular_velocity_state = np.linspace(self.setpoint_angular_velocity + self.angular_velocity_min, 
        self.setpoint_angular_velocity + self.angular_velocity_max, self.discrete_angular_velocities, dtype=int)
        print("Angular velocity states: ", self.angular_velocity_state)
        self.discrete_angular_velocity_state_size = int((self.angular_velocity_max - self.angular_velocity_min) / (self.discrete_angular_velocities - 1))
        print("Discrete angular velocity state size: ", self.discrete_angular_velocity_state_size)

        # Target state
        self.target_state = self.get_discrete_state(self.setpoint_angle, self.setpoint_angular_velocity)
        print(f"Target state: {self.target_state}")

        # Action part
        self.action_min = -action_range
        self.action_max = action_range
        self.discrete_actions = discrete_actions
        self.actions = np.linspace(self.action_min, self.action_max, self.discrete_actions, dtype=int)
        print("Actions: ", self.actions)

        self.q_table = np.random.uniform(low=-2, high=0, size=(len(self.angle_state), len(self.angular_velocity_state), len(self.actions)))
        print("Q-Table: ", np.shape(self.q_table))

        self.ep_rewards = []
        self.aggr_ep_rewards = {'ep': [], 'avg': [], 'min': [], 'max': []}

        self.show_every = show_every

    def get_discrete_state(self, angle, angular_velocity):
        discrete_angle_state = (angle - self.angle_min) * (self.discrete_angles-1) / (self.angle_max - self.angle_min)
        discrete_angular_velocity_state = (angular_velocity - self.angular_velocity_min) * (self.discrete_angular_velocities-1) / (self.angular_velocity_max - self.angular_velocity_min)
        
        if (discrete_angle_state >= self.discrete_angles-1):
            discrete_angle_state = self.discrete_angles - 1
        elif (discrete_angle_state < 0):
            discrete_angle_state = 0

        if (discrete_angular_velocity_state >= self.discrete_angular_velocities-1):
            discrete_angular_velocity_state = self.discrete_angular_velocities - 1
        elif (discrete_angular_velocity_state < 0):
            discrete_angular_velocity_state = 0
        
        return tuple([int(round(discrete_angle_state)), int(round(discrete_angular_velocity_state))])

    def save_qtable(self, filename):
        # Write the array to disk
        with open(filename, 'w') as outfile:
            # I'm writing a header here just for the sake of readability
            # Any line starting with "#" will be ignored by numpy.loadtxt
            outfile.write('# Array shape: {0}\n'.format(self.q_table.shape))
            
            # Iterating through a ndimensional array produces slices along
            # the last axis. This is equivalent to data[i,:,:] in this case
            for data_slice in self.q_table:

                # The formatting string indicates that I'm writing out
                # the values in left-justified columns 7 characters in width
                # with 2 decimal places.  
                np.savetxt(outfile, data_slice, fmt='%-7f')

                # Writing out a break to indicate different slices...
                outfile.write('# New slice\n')


    def read_qtable(self, filename):
        new_data = np.loadtxt(filename)

        # Note that this returned a 2D array!
        #print (new_data.shape)

        # However, going back to 3D is easy if we know the 
        # original shape of the array
        new_data = new_data.reshape((21, 7, 21))
        self.q_table = new_data
        # Just to check that they're the same...
        #assert np.all(new_data == np.around(q.q_table, 6))

if __name__ == "__main__":
    # Test variables
    angle = 4.9
    angular_velocity = 45
    state = [angle, angular_velocity]

    q = QLearningEnv(setpoint_angle=3.587, show_every=10)
    discrete_state = q.get_discrete_state(angle, angular_velocity)
    print(discrete_state)
    print(q.angle_state[discrete_state[0]])
    print(q.angular_velocity_state[discrete_state[1]])
    print(q.q_table[discrete_state])

    for episode in range(q.episodes):

        while (angle < q.setpoint_angle - q.discrete_angle_state_size or angle > q.setpoint_angle + q.discrete_angle_state_size):
            #time.sleep(0.050)
            angle = 3#get_angle()
            print(f"Angle: {angle:9.4f}")
        print("Ready to start episode")

        while (angle > q.setpoint_angle - q.discrete_angle_state_size and angle < q.setpoint_angle + q.discrete_angle_state_size):
            # time.sleep(0.050)
            angle = 5   #get_angle()
            # angular_velocity = get angular velocity
            print(f"Angle: {angle:9.4f}")
        print("Episode started!")

        episode_reward = 0

        discrete_state = q.get_discrete_state(angle, angular_velocity)

        done = False

        while not done:

            if np.random.random() > q.epsilon:
                action = np.argmax(q.q_table[discrete_state])
            else: 
                action = np.random.randint(0, q.discrete_actions)

            angle, angular_velocity, reward, done, _ = q.step(action)
            episode_reward += reward
            new_discrete_state = q.get_discrete_state(angle, angular_velocity)
            if not done:
                max_future_q = np.max(q.q_table[new_discrete_state])
                current_q = q.q_table[discrete_state + (action, )]
                new_q = (1 - q.learning_rate) * current_q + q.learning_rate * (reward + q.discount * max_future_q)
                q.q_table[discrete_state + (action, )] = new_q
            elif (angle > q.setpoint_angle - q.discrete_angle_state_size and angle < q.setpoint_angle + q.discrete_angle_state_size):
                print(f"We made it on espisode: {episode}")
                q.q_table[discrete_state + (action, )] = 0

            discrete_state = new_discrete_state
        
        if q.end_epsilon_decaying >= episode >= q.start_epsilon_decaying:
            q.epsilon -= q.epsilon_decay_value

        q.ep_rewards.append(episode_reward)

        if not episode % q.SHOW_EVERY:
            average_reward = sum(q.ep_rewards[-q.show_every:])/len(q.ep_rewards[-q.show_every:])
            q.aggr_ep_rewards['ep'].append(episode)
            q.aggr_ep_rewards['avg'].append(average_reward)
            q.aggr_ep_rewards['min'].append(min(q.ep_rewards[-q.show_every:]))
            q.aggr_ep_rewards['max'].append(max(q.ep_rewards[-q.show_every:]))

            print(f"Episode: {episode} avg: {average_reward} min: {min(q.ep_rewards[-q.show_every:])} max {max(q.ep_rewards[-q.show_every:])}")
    
    plt.plot(q.aggr_ep_rewards['ep'], q.aggr_ep_rewards['avg'], label="avg")
    plt.plot(q.aggr_ep_rewards['ep'], q.aggr_ep_rewards['min'], label="min")
    plt.plot(q.aggr_ep_rewards['ep'], q.aggr_ep_rewards['max'], label="max")
    plt.legend(loc=4)
    plt.show()