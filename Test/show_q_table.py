import numpy as np
import matplotlib.pyplot as plt

def quadraticspace(discrete_actions):
    a = np.zeros(discrete_actions).astype(int)
    middle = (discrete_actions-1)/2
    for i in range(0, discrete_actions):
        if i < middle:
            a[i] = -pow((i - middle), 2) * 100
        elif i == middle:
            a[i] = 0
        else:
            a[i] = pow((i - middle), 2) * 100
    return a

data = np.loadtxt("Data/q_table25best.txt")
print(np.shape(data))
print(np.shape(data)[0])
H = np.zeros((np.shape(data)))

# Angle min: -7.615411686209791
# Angle max: 12.384588313790209
min_x = -7.82068274
max_x = 12.17931726
a = quadraticspace(21)
for i in range(0,np.shape(data)[0]):
    for j in range(0,np.shape(data)[1]):        
        H[i][j] = (data[i][j] - data.min())/(data.max() - data.min())

fig = plt.figure(figsize=(6, 3.2))

ax = fig.add_subplot(111)
ax.set_title('colorMap')
plt.imshow(H,interpolation="none", cmap='Blues',origin = 'lower') #extent =[-10, 10, min_x, max_x]
ax.set_aspect('equal')
#ax.set_yticks(a)
plt.colorbar(orientation='vertical')
plt.show()