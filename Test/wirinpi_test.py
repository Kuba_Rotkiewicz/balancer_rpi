import wiringpi as wpi
from time import sleep

class stepper_motor():

    BASE_FREQ = 19200000
    PWM_OUTPUT = 2
    PWM_MODE_MS = 0
    OUTPUT = 1

    STEPPER_MOTOR_DIR_RIGHT = 6
    STEPPER_MOTOR_STEP_RIGHT = 12
    STEPPER_MOTOR_EN_RIGHT = 5

    STEPPER_MOTOR_DIR_LEFT = 16
    STEPPER_MOTOR_STEP_LEFT = 13
    STEPPER_MOTOR_EN_LEFT = 26

    def __init__(self):
        wpi.wiringPiSetupGpio()
        wpi.pwmSetMode(PWM_MODE_MS)

        # RIGHT STEPPER MOTOR SETUP
        wpi.pinMode(STEPPER_MOTOR_DIR_RIGHT, OUTPUT)       
        wpi.pinMode(STEPPER_MOTOR_EN_RIGHT, OUTPUT) 
        wpi.pinMode(STEPPER_MOTOR_STEP_RIGHT,PWM_OUTPUT)
        
        # LEFT STEPPER MOTOR SETUP
        wpi.pinMode(STEPPER_MOTOR_DIR_LEFT, OUTPUT)       
        wpi.pinMode(STEPPER_MOTOR_EN_LEFT, OUTPUT) 
        wpi.pinMode(STEPPER_MOTOR_STEP_LEFT,PWM_OUTPUT)

        self.right_disable()
        self.left_disable()
        self.right_forward()
        self.left_forward()

    def right_enable(self):
        wpi.digitalWrite(STEPPER_MOTOR_EN_RIGHT, 0)
    
    def left_enable(self):
        wpi.digitalWrite(STEPPER_MOTOR_EN_LEFT, 0)

    def right_disable(self):
        wpi.digitalWrite(STEPPER_MOTOR_EN_RIGHT, 1)
    
    def left_disable(self):
        wpi.digitalWrite(STEPPER_MOTOR_EN_LEFT, 1)

    def right_forward(self):
        wpi.digitalWrite(STEPPER_MOTOR_DIR_RIGHT, 0)
    
    def left_forward(self):
        wpi.digitalWrite(STEPPER_MOTOR_DIR_LEFT, 1)

    def right_backward(self):
        wpi.digitalWrite(STEPPER_MOTOR_DIR_RIGHT, 1)
    
    def left_backward(self):
        wpi.digitalWrite(STEPPER_MOTOR_DIR_LEFT, 0)
    """
    def set_freq(self, freq):
        period_cycles = 19200000/freq
        prescaler = int(period_cycles / 4095)    #clock
        if prescaler == 0:
            print(freq)
        overflow = int((period_cycles + (prescaler/2))/prescaler)
        duty = int(overflow/2)
        wpi.pwmSetRange(overflow)
        wpi.pwmSetClock(prescaler)
        wpi.pwmWrite(PIN, duty)
    """
    def set_freq1(self, pin, freq):
        if(freq > 0 and freq < 30000):
            period_cycles = BASE_FREQ/freq
            prescaler = 8               #clock
            overflow = int((period_cycles + (prescaler/2))/prescaler)
            duty = int(overflow/2)
            wpi.pwmSetRange(overflow)
            wpi.pwmSetClock(prescaler)
            wpi.pwmWrite(pin, duty)
        elif (freq == 0):
            # Set frequency to 1kHz and duty for 0 -> PWM = 0
            wpi.pwmSetClock(100)
            wpi.pwmSetRange(192)
            wpi.pwmWrite(pin, 0)


if __name__ == "__main__":
    try:
        sm = stepper_motor()

        for i in range(1, 300):
            sm.set_freq1(sm.STEPPER_MOTOR_STEP_LEFT, i*100)
            sleep(0.01)
    except KeyboardInterrupt():
        print("Interrupt")
    finally: 
        sm.set_freq1(sm.STEPPER_MOTOR_STEP_LEFT, i*100)
        wpi.digitalWrite(5, 1)
    """
    wiringpi.pwmSetRange(3841)

    wiringpi.pwmSetClock(5)
    wiringpi.pwmWrite(PIN, 1921)
    wiringpi.pwmSetClock(100)
    wiringpi.pwmSetRange(192)
    wiringpi.pwmWrite(PIN, 96)
    sleep(1)
    """
