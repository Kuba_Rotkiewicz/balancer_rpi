import matplotlib.pyplot as plt
import matplotlib.animation as animation
from mpu6050 import MPU6050
import time
import threading
import numpy as np
from kalman import Kalman
from moving_average_filter import MovingAverageFilter
import csv

def handler():
    global pitch
    global ang_vel
    global filtered
    global angle
    
    SAVE_TO_CSV = 0
    if SAVE_TO_CSV:
        with open('filter_conv.csv','a') as fd:
            #fd.write(f"{pitch},{filtered},{angle_dt},{angle}\n")
            fd.write("Pitch,Filtered,Angle\n")

    mpu = MPU6050()
    k = Kalman(0.0001, 0.00003, 0.2,0)
    f = MovingAverageFilter(22) 
    start = time.time()
    while True:
        pitch = mpu.get_pitch_angle()
        ang_vel = -mpu.get_gyro_y_data()
        filtered = f.moving_average_filter_conv(pitch)
        angle_dt = time.time() - start
        angle = k.getAngle(pitch, ang_vel, angle_dt)
        start = time.time()
        print(f"P:{(pitch):9.4f}, V:{(ang_vel):9.4f}, F: {(filtered):9.4f}, A: {(angle):9.4f}, , A_dt: {(angle_dt):9.4f}")
        time.sleep(0.020)
        if SAVE_TO_CSV:
            with open('filter_conv.csv','a') as fd:
                #fd.write(f"{pitch},{filtered},{angle_dt},{angle}\n")
                fd.write(f"{pitch},{filtered},{angle}\n")

global pitch
pitch = 0
global ang_vel
ang_vel = 0
global filtered
filtered = 0
global angle
angle = 0

# Parameters
x_len = 200         # Number of points to display
y_range = [-40, 40]  # Range of possible Y values to display

# Create figure for plotting
fig = plt.figure()

ax = fig.add_subplot(1, 1, 1)
xs = list(range(0, 200))
ys = [0] * x_len
ax.set_ylim(y_range)
# Create a blank line. We will update the line in animate
line, = ax.plot(xs, ys,'go-', label='line 1', linewidth=0.5, markersize=0.5)

ax1 = fig.add_subplot(1, 1, 1)
xs1 = list(range(0, 200))
ys1 = [0] * x_len
ax1.set_ylim(y_range)
# Create a blank line. We will update the line in animate
line1, = ax1.plot(xs1, ys1, 'rs-', label='line 2', linewidth=0.5, markersize=0.5)

# Add labels
#plt.title('Angle')
#plt.xlabel('Samples')
#plt.ylabel('Angle')

# This function is called periodically from FuncAnimation
def animate(i, ys, ys1):

    # Read temperature (Celsius) from TMP102
    y1 = round(angle, 2)
    y2 = round(pitch, 2)
    # Add y to list
    ys.append(y1)
    ys1.append(y2)

    # Limit y list to set number of items
    ys = ys[-x_len:]
    ys1 = ys1[-x_len:]

    # Update line with new Y values
    line.set_ydata(ys)
    line1.set_ydata(ys1)
    # Add labels
    
    return line,line1,

# Start measurements
t = threading.Thread(target=handler)
t.start()

# Set up plot to call animate() function periodically
ani = animation.FuncAnimation(fig,
    animate,
    fargs=(ys,ys1,),
    interval=20,
    blit=True)
plt.show()