import numpy as np
import sys
import threading
from moving_average_filter import MovingAverageFilter
from mpu6050 import MPU6050
from kalman import Kalman
import csv
import time
import traceback
from simple_pid import PID
import pigpio

READ_ANGLE_FREQUENCY = 200
READ_ANGLE_PERIOD = round(1/READ_ANGLE_FREQUENCY,3)
CALC_PID_FREQUENCY = 1000
CALC_PID_PERIOD = round(1/CALC_PID_FREQUENCY,3)

SAVE_TO_CSV = 0

STEPPER_MOTOR_DIR_RIGHT = 6
STEPPER_MOTOR_STEP_RIGHT = 12
STEPPER_MOTOR_EN_RIGHT = 5

STEPPER_MOTOR_DIR_LEFT = 16
STEPPER_MOTOR_STEP_LEFT = 13
STEPPER_MOTOR_EN_LEFT = 26


def angle_timer_handler(READ_ANGLE_PERIOD, SAVE_TO_CSV):
    global start
    global end
    global offset_angle
    global angle
    offset_angle_array = np.zeros(1)

    while True:
        pitch = mpu.get_pitch_angle()
        angular_velocity = mpu.get_gyro_y_data()
        filtered = f.moving_average_filter_conv(pitch)
        
        time.sleep(READ_ANGLE_PERIOD-0.0025)
        end = time.time()
        dt = end - start
        angle = k.getAngle(filtered, angular_velocity, 0.005)
        #print(f"Angle: {(pitch):9.4f}, Filtered: {(filtered):9.4f}, Time: {(dt):9.4f}, Kalman: {(angle):9.4f}")

        # Initial setpoint angle calibration
        if len(offset_angle_array) < 150:
            offset_angle_array = np.append(offset_angle_array, pitch)
        elif len(offset_angle_array) == 150:
            offset_angle = sum(offset_angle_array[-50:])/50
            offset_angle_array = np.append(offset_angle_array, offset_angle)
            print("Offset Angle: ",offset_angle)

        start = time.time()
        #print(f"A: {(pitch):9.4f}, F: {(filtered):9.4f}, F_conv: {(filtered_conv):9.4f}, T_F: {(t_f):9.5f}, T_C: {(t_c):9.5f}")
        
        if SAVE_TO_CSV:
            with open('filter_conv.csv','a') as fd:
                #fd.write(f"{pitch},{filtered},{dt},{angle}\n")
                fd.write(f"{pitch},{filtered},{filtered_conv}\n")

def pid_timer_handler(CALC_PID_PERIOD, pid_left_wheel_velocity, pid_right_wheel_velocity, stepper_motors):
    global angle
    global offset_angle
    start = 0
    end = 0
    global fall_trigger
    #fall_trigger = 1
    LEFT_WHEEL_ENABLE()
    RIGHT_WHEEL_ENABLE()

    while True:
        start = time.time()

        left_wheel_control_frequency = pid_left_wheel_velocity(angle)
        right_wheel_control_frequency = pid_right_wheel_velocity(angle)

        if (angle > offset_angle and angle < offset_angle + 70):
            if fall_trigger == 0:
                LEFT_WHEEL_ENABLE()
                RIGHT_WHEEL_ENABLE()
                fall_trigger = 1
            LEFT_WHEEL_FREQ(abs(round(left_wheel_control_frequency)))
            RIGHT_WHEEL_FREQ(abs(round(left_wheel_control_frequency)))
            LEFT_WHEEL_FORWARD()
            RIGHT_WHEEL_FORWARD()
        elif (angle < offset_angle and angle > offset_angle - 70):
            if fall_trigger == 0:
                LEFT_WHEEL_ENABLE()
                RIGHT_WHEEL_ENABLE()
                fall_trigger = 1
            LEFT_WHEEL_FREQ(abs(round(left_wheel_control_frequency)))
            RIGHT_WHEEL_FREQ(abs(round(left_wheel_control_frequency)))
            LEFT_WHEEL_BACKWARD()
            RIGHT_WHEEL_BACKWARD()
        else: # (angle > offset_angle + 50 or angle < offset_angle - 50):
            LEFT_WHEEL_DISABLE()
            RIGHT_WHEEL_DISABLE()
            fall_trigger = 0
        end = time.time()
        time.sleep(CALC_PID_PERIOD)
        
        
        dt = end - start
        #print(f"Left: {(left_wheel_control_frequency):9.4f}, Right: {(right_wheel_control_frequency):9.4f}, dt: {(dt):9.7f}")
        


global end
global start 
end = 0
start = READ_ANGLE_PERIOD
global angle
angle = 0
global offset_angle
offset_angle = 0
global fall_trigger
fall_trigger = 1

# Initialize MPU6050
mpu = MPU6050()
# Initialize pitch angle moving average filter
f = MovingAverageFilter(20) 
# Initialize Kalman filter
k = Kalman(0.001, 0.003, 0.03,0)

stepper_motors = pigpio.pi()
LEFT_WHEEL_FORWARD = lambda: stepper_motors.write(STEPPER_MOTOR_DIR_LEFT, 1)
RIGHT_WHEEL_FORWARD = lambda: stepper_motors.write(STEPPER_MOTOR_DIR_RIGHT, 0)
LEFT_WHEEL_ENABLE = lambda: stepper_motors.write(STEPPER_MOTOR_EN_RIGHT, 0)
RIGHT_WHEEL_ENABLE = lambda: stepper_motors.write(STEPPER_MOTOR_EN_LEFT, 0)
LEFT_WHEEL_BACKWARD = lambda: stepper_motors.write(STEPPER_MOTOR_DIR_LEFT, 0)
RIGHT_WHEEL_BACKWARD = lambda: stepper_motors.write(STEPPER_MOTOR_DIR_RIGHT, 1)
LEFT_WHEEL_DISABLE = lambda: stepper_motors.write(STEPPER_MOTOR_EN_RIGHT, 1)
RIGHT_WHEEL_DISABLE = lambda: stepper_motors.write(STEPPER_MOTOR_EN_LEFT, 1)
LEFT_WHEEL_FREQ = lambda x: stepper_motors.hardware_PWM(STEPPER_MOTOR_STEP_LEFT,x,500000)
RIGHT_WHEEL_FREQ = lambda x: stepper_motors.hardware_PWM(STEPPER_MOTOR_STEP_RIGHT,x,500000)

# Initialize thread as timer with an interrupt for the angle calculation
angle_timer = threading.Thread(target=angle_timer_handler, args=(READ_ANGLE_PERIOD, SAVE_TO_CSV))
angle_timer.start()
time.sleep(0.8)
print("Offset Angle: ",offset_angle)

pid_left_wheel_velocity = PID(300.0, 0.0, 0.0, offset_angle, 0.001)
pid_right_wheel_velocity = PID(300.0, 0.0, 0.0, offset_angle, 0.001)
pid_left_wheel_velocity.output_limits = (-28000, 28000)
pid_right_wheel_velocity.output_limits = (-28000, 28000)

pid_timer = threading.Thread(target=pid_timer_handler, args=(CALC_PID_PERIOD, pid_left_wheel_velocity, pid_right_wheel_velocity, stepper_motors))
pid_timer.start()
try:
    while True:
        print("Working")
except KeyboardInterrupt():
    print("Interrupt!")
finally:
    LEFT_WHEEL_FREQ(0)
    RIGHT_WHEEL_FREQ(0)
    LEFT_WHEEL_DISABLE()
    RIGHT_WHEEL_DISABLE()
    sys.exit()
"""
try:
    # Initialize thread as timer with an interrupt for the angle calculation
    angle_timer = threading.Thread(target=angle_timer_handler, args=(READ_ANGLE_PERIOD, SAVE_TO_CSV))
    angle_timer.start()
    time.sleep(0.8)
    print("Offset Angle: ",offset_angle)

    pid_left_wheel_velocity = PID(500.0, 0.0, 0.0, offset_angle, 0.001)
    pid_right_wheel_velocity = PID(500.0, 0.0, 0.0, offset_angle, 0.001)
    pid_left_wheel_velocity.output_limits = (-28000, 28000)
    pid_right_wheel_velocity.output_limits = (-28000, 28000)

    pid_timer = threading.Thread(target=pid_timer_handler, args=(CALC_PID_PERIOD, pid_left_wheel_velocity, pid_right_wheel_velocity, stepper_motors))
    pid_timer.start()

except Exception() as ex:
    print("Error message: ", ex)
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_tb(exc_traceback)
finally:
    LEFT_WHEEL_FREQ(0)
    RIGHT_WHEEL_FREQ(0)
    LEFT_WHEEL_DISABLE()
    RIGHT_WHEEL_DISABLE()
    sys.exit()
"""