import numpy as np

class Kalman:
    def __init__(self, Q_angle = 0, Q_bias = 0, R_measure = 0, angle = 0):    #[[0] * 2 for i in range(2)]
        """ Kalman filter variables """
        self.Q_angle = Q_angle              # Process noise variance for the accelerometer
        self.Q_bias = Q_bias                # Process noise variance for the gyro bias
        self.R_measure = R_measure          # Measurement noise variance - this is actually 
                                            # the variance of the measurement noise
        self.angle = angle                  # The angle calculated by the Kalman filter - part of the 2x1 state vector
        self.bias = 0                    # The gyro bias calculated by the Kalman filter - part of the 2x1 state vector
        self.rate = 0                    # Unbiased rate calculated from the rate and the calculated bias - 
                                            # you have to call getAngle to update the rate
        self.P = np.zeros((2,2))            # Error covariance matrix - This is a 2x2 matrix
        #P = np.zeros((2,2))                 # Error covariance matrix - This is a 2x2 matrix

    def getAngle(self, newAngle, newRate, dt):
        """ 
        Discrete Kalman filter time update equations - Time Update ("Predict")
        Update xhat - Project the state ahead
        Step 1 
        """
        self.rate = newRate - self.bias
        self.angle += dt * self.rate

        """
        Update estimation error covariance - Project the error covariance ahead
        Step 2
        """
        self.P[0][0] += dt * (dt*self.P[1][1] - self.P[0][1] - self.P[1][0] + self.Q_angle)
        self.P[0][1] -= dt * self.P[1][1]
        self.P[1][0] -= dt * self.P[1][1]
        self.P[1][1] += self.Q_bias * dt

        """
        Discrete Kalman filter measurement update equations - Measurement Update ("Correct")
        Calculate Kalman gain - Compute the Kalman gain
        Step 4 
        """
        S = self.P[0][0] + self.R_measure;  # Estimate error
        
        """
        Step 5
        """
        K = [0,0]                           # Kalman gain - This is a 2x1 vector
        K[0] = self.P[0][0] / S
        K[1] = self.P[1][0] / S

        """
        Calculate angle and bias - Update estimate with measurement zk (newAngle)
        Step 3 
        """
        y = newAngle - self.angle           # Angle difference
        
        """
        Step 6
        """
        self.angle += K[0] * y
        self.bias += K[1] * y

        """
        Calculate estimation error covariance - Update the error covariance
        Step 7
        """

        P00_temp = self.P[0][0]
        P01_temp = self.P[0][1]

        self.P[0][0] -= K[0] * P00_temp
        self.P[0][1] -= K[0] * P01_temp
        self.P[1][0] -= K[1] * P00_temp
        self.P[1][1] -= K[1] * P01_temp

        return self.angle

